/*
 * Copyright (C) 2023 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */

// TODO: move this to external configuration & stapps core

import {
  SCAuthorizationProviderType,
  SCThing,
  SCThingOriginType,
  SCThingRemoteOrigin,
  SCThingType,
} from '@openstapps/core';
import {SCIcon} from '../app/util/ion-icon/icon';

export const SCSectionThingType = 'section' as SCThingType;
export const SCSectionLinkThingType = 'section link' as SCThingType;

const StubOrigin: SCThingRemoteOrigin = {
  type: SCThingOriginType.Remote,
  name: 'todo',
  indexed: new Date(Date.now()).toISOString(),
};

const SCSectionConstantValues: Pick<SCSection, 'type' | 'origin' | 'uid'> = {
  type: SCSectionThingType,
  origin: StubOrigin,
  uid: 'stub',
};

const SCSectionLinkConstantValues: Pick<SCSection, 'type' | 'origin' | 'uid'> = {
  type: SCSectionLinkThingType,
  origin: StubOrigin,
  uid: 'stub',
};

export interface SCSectionLink extends SCThing {
  link: string[];
  needsAuth?: true;
  icon?: string;
}

export interface SCSection extends SCThing {
  authProvider?: SCAuthorizationProviderType;
  links: SCSectionLink[];
}

export const profilePageSections: SCSection[] = [
  {
    name: '',
    links: [
      {
        name: 'Favorites',
        icon: SCIcon`grade`,
        link: ['/favorites'],
        translations: {
          de: {
            name: 'Favoriten',
          },
        },
        ...SCSectionLinkConstantValues,
      },
      {
        name: 'Schedule',
        icon: SCIcon`calendar_today`,
        link: ['/schedule'],
        translations: {
          de: {
            name: 'Stundenplan',
          },
        },
        ...SCSectionLinkConstantValues,
      },
      {
        name: 'Course Catalog',
        icon: SCIcon`inventory_2`,
        link: ['/catalog'],
        translations: {
          de: {
            name: 'Vorlesungs-verzeichnis',
          },
        },
        ...SCSectionLinkConstantValues,
      },
      {
        name: 'Settings',
        icon: SCIcon`settings`,
        link: ['/settings'],
        translations: {
          de: {
            name: 'Einstellungen',
          },
        },
        ...SCSectionLinkConstantValues,
      },
      {
        name: 'Feedback',
        icon: SCIcon`rate_review`,
        link: ['/feedback'],
        translations: {
          de: {
            name: 'Feedback',
          },
        },
        ...SCSectionLinkConstantValues,
      },
      {
        name: 'About',
        icon: SCIcon`info`,
        link: ['/about'],
        translations: {
          de: {
            name: 'Über die App',
          },
        },
        ...SCSectionLinkConstantValues,
      },
    ],
    translations: {
      de: {
        name: '',
      },
    },
    ...SCSectionConstantValues,
  },
  {
    name: 'Campus Services',
    authProvider: 'default',
    links: [
      {
        name: 'Assessments',
        icon: SCIcon`fact_check`,
        link: ['/assessments'],
        needsAuth: true,
        translations: {
          de: {
            name: 'Noten',
          },
        },
        ...SCSectionLinkConstantValues,
      },
    ],
    translations: {
      de: {
        name: 'Campus Dienste',
      },
    },
    ...SCSectionConstantValues,
  },
  {
    name: 'Library',
    authProvider: 'paia',
    links: [
      {
        name: 'Library Catalog',
        icon: SCIcon`local_library`,
        link: ['/hebis-search'],
        translations: {
          de: {
            name: 'Bibliotheks-katalog',
          },
        },
        ...SCSectionLinkConstantValues,
      },
      {
        name: 'Library Account',
        icon: SCIcon`badge`,
        needsAuth: true,
        link: ['/library-account/profile'],
        translations: {
          de: {
            name: 'Bibliotheks-konto',
          },
        },
        ...SCSectionLinkConstantValues,
      },
      {
        name: 'Orders & Reservations',
        icon: SCIcon`collections_bookmark`,
        needsAuth: true,
        link: ['/library-account/holds'],
        translations: {
          de: {
            name: 'Bestellungen & Vormerkungen',
          },
        },
        ...SCSectionLinkConstantValues,
      },
      {
        name: 'Checked out items',
        icon: SCIcon`library_books`,
        needsAuth: true,
        link: ['/library-account/checked-out'],
        translations: {
          de: {
            name: 'Deine Ausleihen',
          },
        },
        ...SCSectionLinkConstantValues,
      },
      {
        name: 'Fines',
        icon: SCIcon`request_page`,
        needsAuth: true,
        link: ['/library-account/fines'],
        translations: {
          de: {
            name: 'Gebühren',
          },
        },
        ...SCSectionLinkConstantValues,
      },
    ],
    translations: {
      de: {
        name: 'Bibliothek',
      },
    },
    ...SCSectionConstantValues,
  },
];
