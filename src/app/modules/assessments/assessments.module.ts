/*
 * Copyright (C) 2022 StApps
 *  This program is free software: you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the Free
 *  Software Foundation, version 3.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 *  more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  this program. If not, see <https://www.gnu.org/licenses/>.
 */

import {NgModule} from '@angular/core';
import {AssessmentListItemComponent} from './types/assessment/assessment-list-item.component';
import {AssessmentBaseInfoComponent} from './types/assessment/assessment-base-info.component';
import {AssessmentDetailComponent} from './types/assessment/assessment-detail.component';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {IonicModule} from '@ionic/angular';
import {TranslateModule} from '@ngx-translate/core';
import {DataModule} from '../data/data.module';
import {ThingTranslateModule} from '../../translation/thing-translate.module';
import {CourseOfStudyAssessmentComponent} from './types/course-of-study/course-of-study-assessment.component';
import {AssessmentsPageComponent} from './page/assessments-page.component';
import {RouterModule} from '@angular/router';
import {AuthGuardService} from '../auth/auth-guard.service';
import {MomentModule} from 'ngx-moment';
import {AssessmentsListItemComponent} from './list/assessments-list-item.component';
import {AssessmentsDataListComponent} from './list/assessments-data-list.component';
import {AssessmentsDetailComponent} from './detail/assessments-detail.component';
import {AssessmentsProvider} from './assessments.provider';
import {AssessmentsSimpleDataListComponent} from './list/assessments-simple-data-list.component';
import {ProtectedRoutes} from '../auth/protected.routes';
import {AssessmentsTreeListComponent} from './list/assessments-tree-list.component';
import {IonIconModule} from '../../util/ion-icon/ion-icon.module';
import {UtilModule} from '../../util/util.module';

const routes: ProtectedRoutes = [
  {
    path: 'assessments',
    component: AssessmentsPageComponent,
    data: {authProvider: 'default'},
    canActivate: [AuthGuardService],
  },
  {
    path: 'assessments/detail/:uid',
    component: AssessmentsDetailComponent,
    data: {authProvider: 'default'},
    canActivate: [AuthGuardService],
  },
];

@NgModule({
  declarations: [
    AssessmentListItemComponent,
    AssessmentBaseInfoComponent,
    AssessmentDetailComponent,
    AssessmentsListItemComponent,
    AssessmentsTreeListComponent,
    CourseOfStudyAssessmentComponent,
    AssessmentsPageComponent,
    AssessmentsDataListComponent,
    AssessmentsDetailComponent,
    AssessmentsSimpleDataListComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    IonIconModule,
    IonicModule,
    RouterModule.forChild(routes),
    TranslateModule,
    DataModule,
    ThingTranslateModule,
    MomentModule,
    UtilModule,
  ],
  providers: [AssessmentsProvider],
  exports: [],
})
export class AssessmentsModule {}
