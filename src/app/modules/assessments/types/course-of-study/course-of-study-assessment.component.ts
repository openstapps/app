/*
 * Copyright (C) 2022 StApps
 *  This program is free software: you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the Free
 *  Software Foundation, version 3.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 *  more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  this program. If not, see <https://www.gnu.org/licenses/>.
 */

import {Component, Input} from '@angular/core';
import {SCAssessment, SCCourseOfStudyWithoutReferences} from '@openstapps/core';

@Component({
  selector: 'course-of-study-assessment',
  templateUrl: 'course-of-study-assessment.html',
  styleUrls: ['course-of-study-assessment.scss'],
})
export class CourseOfStudyAssessmentComponent {
  @Input() courseOfStudy: SCCourseOfStudyWithoutReferences | null;

  _assessments: Promise<SCAssessment[]>;

  @Input() set assessments(value: SCAssessment[]) {
    this._assessments = Promise.resolve(value);
  }
}
