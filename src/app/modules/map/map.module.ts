/*
 * Copyright (C) 2022 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {RouterModule, Routes} from '@angular/router';
import {LeafletModule} from '@asymmetrik/ngx-leaflet';
import {LeafletMarkerClusterModule} from '@asymmetrik/ngx-leaflet-markercluster';
import {IonicModule} from '@ionic/angular';
import {TranslateModule} from '@ngx-translate/core';
import {Polygon} from 'geojson';
import {ThingTranslateModule} from '../../translation/thing-translate.module';
import {ConfigProvider} from '../config/config.provider';
import {DataFacetsProvider} from '../data/data-facets.provider';
import {DataModule} from '../data/data.module';
import {DataProvider} from '../data/data.provider';
import {StAppsWebHttpClient} from '../data/stapps-web-http-client.provider';
import {MenuModule} from '../menu/menu.module';
import {MapProvider} from './map.provider';
import {MapPageComponent} from './page/map-page.component';
import {MapListModalComponent} from './page/modals/map-list-modal.component';
import {MapSingleModalComponent} from './page/modals/map-single-modal.component';
import {MapItemComponent} from './item/map-item.component';
import {NgModule} from '@angular/core';
import {UtilModule} from '../../util/util.module';
import {IonIconModule} from '../../util/ion-icon/ion-icon.module';

/**
 * Initializes the default area to show in advance (before components are initialized)
 *
 * @param configProvider An instance of the ConfigProvider to read the campus polygon from
 * @param mapProvider An instance of the MapProvider to set the default polygon (area to show on the map)
 */
export function initMapConfigFactory(configProvider: ConfigProvider, mapProvider: MapProvider) {
  return async () => {
    mapProvider.defaultPolygon = (await configProvider.getValue('campusPolygon')) as Polygon;
  };
}

const mapRoutes: Routes = [
  {path: 'map', component: MapPageComponent},
  {path: 'map/:uid', component: MapPageComponent},
];

/**
 * Module containing map related stuff
 */
@NgModule({
  declarations: [MapPageComponent, MapListModalComponent, MapSingleModalComponent, MapItemComponent],
  exports: [],
  imports: [
    CommonModule,
    IonicModule.forRoot(),
    LeafletModule,
    IonIconModule,
    LeafletMarkerClusterModule,
    RouterModule.forChild(mapRoutes),
    TranslateModule.forChild(),
    MenuModule,
    DataModule,
    FormsModule,
    ThingTranslateModule,
    UtilModule,
  ],
  providers: [Geolocation, MapProvider, DataProvider, DataFacetsProvider, StAppsWebHttpClient],
})
export class MapModule {}
