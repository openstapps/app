/*
 * Copyright (C) 2021 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {Component, EventEmitter, Input, Output} from '@angular/core';
import {SCPlace} from '@openstapps/core';
import {IonRouterOutlet} from '@ionic/angular';

@Component({
  selector: 'stapps-map-item',
  templateUrl: './map-item.component.html',
  styleUrls: ['./map-item.component.scss'],
})
export class MapItemComponent {
  /**
   * An item to show
   */
  @Input() item: SCPlace;

  // eslint-disable-next-line @angular-eslint/no-output-on-prefix
  @Output() onClose = new EventEmitter<void>();

  constructor(readonly routerOutlet: IonRouterOutlet) {}

  /**
   * Action when edit is clicked
   */
  onCloseClick() {
    this.onClose.emit();
  }
}
