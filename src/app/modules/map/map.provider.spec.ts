/*
 * Copyright (C) 2023 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {TestBed} from '@angular/core/testing';

import {MapProvider} from './map.provider';
import {StAppsWebHttpClient} from '../data/stapps-web-http-client.provider';
import {HttpClientModule} from '@angular/common/http';
import {StorageProvider} from '../storage/storage.provider';
import {MapModule} from './map.module';
import {StorageModule} from '../storage/storage.module';
import {LoggerModule, NGXLogger, NgxLoggerLevel} from 'ngx-logger';
import {ConfigProvider} from '../config/config.provider';
import {sampleDefaultPolygon} from '../../_helpers/data/sample-configuration';
import {RouterModule} from '@angular/router';

describe('MapProvider', () => {
  let provider: MapProvider;
  let configProvider: jasmine.SpyObj<ConfigProvider>;

  beforeEach(() => {
    configProvider = jasmine.createSpyObj('ConfigProvider', ['getValue']);
    TestBed.configureTestingModule({
      imports: [
        MapModule,
        HttpClientModule,
        StorageModule,
        LoggerModule.forRoot({level: NgxLoggerLevel.TRACE}),
        RouterModule.forRoot([]),
      ],
      providers: [
        {
          provide: ConfigProvider,
          useValue: configProvider,
        },
        StAppsWebHttpClient,
        StorageProvider,
        NGXLogger,
      ],
    });

    configProvider.getValue.and.returnValue(sampleDefaultPolygon);
    provider = TestBed.inject(MapProvider);
  });

  it('should be created', () => {
    expect(provider).toBeTruthy();
  });
});
