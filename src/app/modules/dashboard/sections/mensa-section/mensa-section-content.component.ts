/*
 * Copyright (C) 2023 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {ChangeDetectionStrategy, Component, Input} from '@angular/core';
import {SCDish, SCPlace, SCThings} from '@openstapps/core';
import {PlaceMensaService} from '../../../data/types/place/special/mensa/place-mensa-service';
import moment from 'moment';
import {fadeAnimation} from '../../fade.animation';

/**
 * Shows a section with meals of the chosen mensa
 */
@Component({
  selector: 'stapps-mensa-section-content',
  templateUrl: 'mensa-section-content.component.html',
  styleUrls: ['mensa-section-content.component.scss'],
  animations: [fadeAnimation],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MensaSectionContentComponent {
  /**
   * Map of dishes for each day
   */
  // eslint-disable-next-line unicorn/no-null
  dishes: Promise<SCDish[]>;

  @Input() set item(value: SCThings) {
    if (!value) return;
    this.dishes = this.mensaService.getAllDishes(value as SCPlace, 1).then(it => {
      const closestDayWithDishes = Object.keys(it)
        .filter(key => it[key].length > 0)
        .find(key => moment(key).isSame(moment(), 'day'));
      return closestDayWithDishes ? it[closestDayWithDishes] : [];
    });
  }

  constructor(private readonly mensaService: PlaceMensaService) {}
}
