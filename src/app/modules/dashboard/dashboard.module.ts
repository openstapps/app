/*
 * Copyright (C) 2023 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {RouterModule, Routes} from '@angular/router';
import {IonicModule} from '@ionic/angular';
import {SwiperModule} from 'swiper/angular';
import {TranslateModule, TranslatePipe} from '@ngx-translate/core';
import {MomentModule} from 'ngx-moment';
import {DataModule} from '../data/data.module';
import {SettingsProvider} from '../settings/settings.provider';
import {DashboardComponent} from './dashboard.component';
import {SearchSectionComponent} from './sections/search-section/search-section.component';
import {NewsSectionComponent} from './sections/news-section/news-section.component';
import {MensaSectionComponent} from './sections/mensa-section/mensa-section.component';
import {MensaSectionContentComponent} from './sections/mensa-section/mensa-section-content.component';
import {FavoritesSectionComponent} from './sections/favorites-section/favorites-section.component';
import {ThingTranslateModule} from '../../translation/thing-translate.module';
import {UtilModule} from '../../util/util.module';
import {IonIconModule} from '../../util/ion-icon/ion-icon.module';
import {NewsModule} from '../news/news.module';

const catalogRoutes: Routes = [
  {
    path: 'overview',
    component: DashboardComponent,
  },
];

/**
 * Catalog Module
 */
@NgModule({
  declarations: [
    SearchSectionComponent,
    NewsSectionComponent,
    MensaSectionComponent,
    MensaSectionContentComponent,
    FavoritesSectionComponent,
    DashboardComponent,
  ],
  imports: [
    IonicModule.forRoot(),
    IonIconModule,
    FormsModule,
    TranslateModule.forChild(),
    RouterModule.forChild(catalogRoutes),
    CommonModule,
    MomentModule,
    DataModule,
    SwiperModule,
    ThingTranslateModule.forChild(),
    UtilModule,
    NewsModule,
  ],
  providers: [SettingsProvider, TranslatePipe],
})
export class DashboardModule {}
