/*
 * Copyright (C) 2023 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {Component, Input, OnInit} from '@angular/core';
import moment from 'moment';
import {ScheduleProvider} from '../../../calendar/schedule.provider';
import {ScheduleEvent} from '../schema/schema';

/**
 * Component that can display a schedule event
 */
@Component({
  selector: 'stapps-schedule-card',
  templateUrl: 'schedule-card.html',
  styleUrls: ['../../../data/list/data-list-item.scss', 'schedule-card.scss'],
})
export class ScheduleCardComponent implements OnInit {
  cardColor = {
    isExercise: false,
    isLecture: false,
    isDefault: false,
  };

  /**
   * The hour from which on the schedule is displayed
   */
  @Input() fromHour = 0;

  /**
   * Card Y start position
   */
  fromY = 0;

  /**
   * Card Y end position
   */
  height = 0;

  /**
   * Show the card without a top offset
   */
  @Input() noOffset = false;

  /**
   * The scale of the schedule
   */
  @Input() scale = 1;

  /**
   * The event
   */
  @Input() scheduleEvent: ScheduleEvent;

  /**
   * Card shows the name of the place the event takes place
   */
  @Input() showPlaceName = false;

  /**
   * The title of the event
   */
  title: string;

  constructor(private readonly scheduleProvider: ScheduleProvider) {}

  /**
   * Get the note text
   */
  getNote(): string | undefined {
    return this.scheduleEvent?.dateSeries?.name ?? undefined;
  }

  /**
   * Initializer
   */
  ngOnInit() {
    this.fromY = this.noOffset ? 0 : this.scheduleEvent.time.start;
    this.height = moment.duration(this.scheduleEvent.time.duration).asHours();

    this.title = this.scheduleEvent.dateSeries.event.name;

    this.cardColor = {
      isLecture: false,
      isExercise: false,
      isDefault: false,
    };

    switch (
      'categories' in this.scheduleEvent.dateSeries.event
        ? this.scheduleEvent.dateSeries.event?.categories[0]
        : ''
    ) {
      case 'lecture':
        this.cardColor.isLecture = true;
        break;
      case 'exercise':
        this.cardColor.isExercise = true;
        break;
      default:
        this.cardColor.isDefault = true;
    }
  }

  /**
   * Remove the event
   */
  removeEvent(): false {
    if (confirm('Remove event?')) {
      this.scheduleProvider.partialEvents$.next(
        this.scheduleProvider.partialEvents$.value.filter(it => it.uid !== this.scheduleEvent.dateSeries.uid),
      );
    }

    // to prevent event propagation
    return false;
  }
}
