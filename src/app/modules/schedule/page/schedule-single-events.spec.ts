/*
 * Copyright (C) 2022 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */

import {SCDateSeries} from '@openstapps/core';
import {ScheduleSingleEventsComponent} from './schedule-single-events.component';
import moment from 'moment';

describe('ScheduleSingleEvents', () => {
  it('should group date series to days', () => {
    const events: Partial<SCDateSeries>[] = [
      {
        dates: ['2021-12-24T10:00Z', '2021-12-24T12:00Z'],
        duration: 'A',
      },
      {
        dates: ['2021-12-20T10:00Z'],
        duration: 'B',
      },
      {
        dates: ['2021-12-24T10:15Z'],
        duration: 'C',
      },
    ];

    const grouped = ScheduleSingleEventsComponent.groupDateSeriesToDays(events as SCDateSeries[]);
    const seriesToDate = (series: Partial<SCDateSeries>, index: number) => {
      const time = moment(series.dates?.[index]);

      return {
        day: time.clone().startOf('day').toISOString(),
        event: {
          dateSeries: series as SCDateSeries,
          time: {
            start: time.hour() + time.minute() / 60,
            startAsString: moment(time).format('LT'),
            duration: series.duration as string,
            endAsString: moment(time).add(series.duration?.[index]).format('LT'),
          },
        },
      };
    };

    expect(grouped).toEqual([
      [seriesToDate(events[1], 0)],
      [seriesToDate(events[0], 0), seriesToDate(events[2], 0), seriesToDate(events[0], 1)],
    ]);
  });
});
