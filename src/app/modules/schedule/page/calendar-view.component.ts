/*
 * Copyright (C) 2023 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {AfterViewInit, Component, Input, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import moment from 'moment';
import {materialFade, materialManualFade, materialSharedAxisX} from '../../../animation/material-motion';
import {ScheduleResponsiveBreakpoint} from './schema/schema';
import {ScheduleProvider} from '../../calendar/schedule.provider';
import {CalendarComponent} from './components/calendar.component';
import {CalendarService} from '../../calendar/calendar.service';
import {InfiniteSwiperComponent} from './grid/infinite-swiper.component';
import {IonContent} from '@ionic/angular';

/**
 * Component that displays the schedule
 */
@Component({
  selector: 'stapps-calendar-view',
  templateUrl: 'calendar-view.html',
  styleUrls: ['calendar-view.scss', './components/calendar-component.scss'],
  animations: [materialFade, materialSharedAxisX, materialManualFade],
})
export class CalendarViewComponent extends CalendarComponent implements OnInit, OnDestroy, AfterViewInit {
  @ViewChild('mainSwiper') mainSwiper: InfiniteSwiperComponent;

  @ViewChild('headerSwiper') headerSwiper: InfiniteSwiperComponent;

  @ViewChild('content') content: IonContent;

  /**
   * Layout of the schedule
   */
  @Input() layout: ScheduleResponsiveBreakpoint;

  /**
   * Vertical scale of the schedule (distance between hour lines)
   */
  scale = 70;

  /**
   * For use in templates
   */
  moment = moment;

  constructor(
    activatedRoute: ActivatedRoute,
    calendarService: CalendarService,
    scheduleProvider: ScheduleProvider,
  ) {
    super(activatedRoute, calendarService, scheduleProvider);
    // This could be done directly on the properties too instead of
    // here in the constructor, but because of TSLint member ordering,
    // some properties wouldn't be initialized, and if you disable
    // member ordering, auto-fixing the file can still cause reordering
    // of properties.
    const hoursAmount = this.hoursRange.to - this.hoursRange.from + 1;
    this.hours = [...Array.from({length: hoursAmount}).keys()];
  }

  /**
   * Initialize
   */
  ngOnInit() {
    super.onInit();
    if (this.calendarServiceSubscription) {
      this.calendarServiceSubscription.unsubscribe();
    }
    this.calendarServiceSubscription = this.calendarService.goToDateClicked.subscribe(async newIndex => {
      await this.mainSwiper.goToIndex(newIndex);
      this.setDateRange(newIndex);
      await this.scrollCursorIntoView(this.content);
    });
  }

  ngAfterViewInit() {
    void this.scrollCursorIntoView(this.content);
  }

  /**
   * OnDestroy
   */
  ngOnDestroy(): void {
    super.onDestroy();
  }

  /**
   * Load events
   */
  async loadEvents(): Promise<number> {
    await super.loadEvents();

    return this.todaySlideIndex;
  }
}
