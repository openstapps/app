/*
 * Copyright (C) 2023 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {RouterModule, Routes} from '@angular/router';
import {IonicModule} from '@ionic/angular';
import {TranslateModule} from '@ngx-translate/core';
import {ScheduleCardComponent} from './page/grid/schedule-card.component';

import {DateFormatPipe, MomentModule} from 'ngx-moment';
import {UtilModule} from '../../util/util.module';
import {DataModule} from '../data/data.module';
import {DataProvider} from '../data/data.provider';
import {CalendarViewComponent} from './page/calendar-view.component';
import {ScheduleCursorComponent} from './page/grid/schedule-cursor.component';
import {SchedulePageComponent} from './page/schedule-page.component';
import {ScheduleSingleEventsComponent} from './page/schedule-single-events.component';
import {ScheduleViewComponent} from './page/schedule-view.component';
import {ScheduleProvider} from '../calendar/schedule.provider';
import {SwiperModule} from 'swiper/angular';
import {ScheduleDayComponent} from './page/grid/schedule-day.component';
import {ThingTranslateModule} from '../../translation/thing-translate.module';
import {InfiniteSwiperComponent} from './page/grid/infinite-swiper.component';
import {CalendarComponent} from './page/components/calendar.component';
import {IonIconModule} from '../../util/ion-icon/ion-icon.module';
import {ChooseEventsPageComponent} from './page/choose-events-page.component';

const settingsRoutes: Routes = [
  {path: 'schedule', redirectTo: 'schedule/calendar/now'},
  {path: 'schedule/calendar', redirectTo: 'schedule/calendar/now'},
  {path: 'schedule/recurring', redirectTo: 'schedule/recurring/now'},
  {path: 'schedule/single', redirectTo: 'schedule/single/now'},
  // calendar | recurring | single
  {path: 'schedule/:mode/:date', component: SchedulePageComponent},
  // TODO: this is temporary until the new generalized search page is finished
  {path: 'schedule/:mode/:date/event-picker', component: ChooseEventsPageComponent},
];

/**
 * Schedule Module
 */
@NgModule({
  declarations: [
    CalendarComponent,
    CalendarViewComponent,
    ChooseEventsPageComponent,
    ScheduleCardComponent,
    ScheduleCursorComponent,
    SchedulePageComponent,
    ScheduleSingleEventsComponent,
    ScheduleDayComponent,
    ScheduleViewComponent,
    InfiniteSwiperComponent,
  ],
  imports: [
    CommonModule,
    DataModule,
    FormsModule,
    IonicModule.forRoot(),
    IonIconModule,
    MomentModule,
    RouterModule.forChild(settingsRoutes),
    SwiperModule,
    TranslateModule.forChild(),
    UtilModule,
    ThingTranslateModule,
  ],
  providers: [ScheduleProvider, DataProvider, DateFormatPipe],
})
export class ScheduleModule {}
