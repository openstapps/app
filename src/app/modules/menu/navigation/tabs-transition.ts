/*
 * Copyright (C) 2022 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */

import type {AnimationBuilder} from '@ionic/angular';
import {AnimationController} from '@ionic/angular';
import type {AnimationOptions} from '@ionic/angular/providers/nav-controller';

/**
 *
 */
export function tabsTransition(animationController: AnimationController): AnimationBuilder {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  return (_baseElement: HTMLElement, options: AnimationOptions | any) => {
    const duration = options.duration || 350;
    const contentExitDuration = options.contentExitDuration || 100;

    const rootTransition = animationController.create().duration(duration);

    const enterTransition = animationController
      .create()
      .fromTo('opacity', '1', '1')
      .addElement(options.enteringEl);
    const exitZIndex = animationController
      .create()
      .beforeStyles({zIndex: 0})
      .afterClearStyles(['zIndex'])
      .addElement(options.leavingEl);
    const exitTransition = animationController
      .create()
      .duration(contentExitDuration * 2)
      .easing('cubic-bezier(0.87, 0, 0.13, 1)')
      .fromTo('opacity', '1', '0')
      .addElement(options.leavingEl.querySelector('ion-header'));
    const contentExit = animationController
      .create()
      .easing('linear')
      .duration(contentExitDuration)
      .fromTo('opacity', '1', '0')
      .addElement(options.leavingEl.querySelectorAll(':scope > *:not(ion-header)'));
    const contentEnter = animationController
      .create()
      .delay(contentExitDuration)
      .duration(duration - contentExitDuration)
      .easing('cubic-bezier(0.16, 1, 0.3, 1)')
      .fromTo('transform', 'scale(1.025)', 'scale(1)')
      .fromTo('opacity', '0', '1')
      .addElement(options.enteringEl.querySelectorAll(':scope > *:not(ion-header)'));

    rootTransition.addAnimation([enterTransition, contentExit, contentEnter, exitTransition, exitZIndex]);
    return rootTransition;
  };
}
