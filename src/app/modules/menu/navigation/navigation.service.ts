/*
 * Copyright (C) 2022 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */

import {Injectable} from '@angular/core';
import {SCAppConfigurationMenuCategory} from '@openstapps/core';
import {ConfigProvider} from '../../config/config.provider';
import {NGXLogger} from 'ngx-logger';

@Injectable({
  providedIn: 'root',
})
export class NavigationService {
  constructor(private configProvider: ConfigProvider, private logger: NGXLogger) {}

  async getMenu() {
    let menu: SCAppConfigurationMenuCategory[] = [];
    try {
      menu = this.configProvider.getValue('menus') as SCAppConfigurationMenuCategory[];
    } catch (error) {
      this.logger.error(`error from loading menu entries: ${error}`);
    }

    return menu;
  }
}
