/*
 * Copyright (C) 2023 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {NgModule} from '@angular/core';
import {RootLinkDirective} from './root-link.directive';
import {NavigationComponent} from './navigation.component';
import {TabsComponent} from './tabs.component';
import {CommonModule} from '@angular/common';
import {IonicModule} from '@ionic/angular';
import {IonIconModule} from '../../../util/ion-icon/ion-icon.module';
import {TranslateModule} from '@ngx-translate/core';
import {RouterModule} from '@angular/router';
import {OfflineNoticeComponent} from './offline-notice.component';

@NgModule({
  declarations: [RootLinkDirective, NavigationComponent, TabsComponent, OfflineNoticeComponent],
  imports: [CommonModule, IonicModule, IonIconModule, TranslateModule, RouterModule],
  exports: [TabsComponent, RootLinkDirective, NavigationComponent],
})
export class NavigationModule {}
