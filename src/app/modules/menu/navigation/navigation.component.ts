/*
 * Copyright (C) 2023 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {Component, OnInit} from '@angular/core';
import {LangChangeEvent, TranslateService} from '@ngx-translate/core';
import {
  SCAppConfigurationMenuCategory,
  SCLanguage,
  SCThingTranslator,
  SCTranslations,
} from '@openstapps/core';
import {NavigationService} from './navigation.service';
import config from 'capacitor.config';
import {SettingsProvider} from '../../settings/settings.provider';
import {BreakpointObserver} from '@angular/cdk/layout';

/**
 * Generated class for the MenuPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
@Component({
  selector: 'stapps-navigation',
  styleUrls: ['navigation.scss'],
  templateUrl: 'navigation.html',
})
export class NavigationComponent implements OnInit {
  showTabbar = true;

  /**
   * Name of the app
   */
  appName = config.appName;

  /**
   * Possible languages to be used for translation
   */
  language: keyof SCTranslations<SCLanguage>;

  /**
   * Menu entries from config module
   */
  menu: SCAppConfigurationMenuCategory[];

  /**
   * Core translator
   */
  translator: SCThingTranslator;

  constructor(
    public translateService: TranslateService,
    private navigationService: NavigationService,
    private settingsProvider: SettingsProvider,
    private responsive: BreakpointObserver,
  ) {
    translateService.onLangChange.subscribe((event: LangChangeEvent) => {
      this.language = event.lang as keyof SCTranslations<SCLanguage>;
      this.translator = new SCThingTranslator(this.language);
    });

    this.responsive.observe(['(min-width: 768px)']).subscribe(result => {
      this.showTabbar = !result.matches;
    });
  }

  async ngOnInit() {
    this.language = (await this.settingsProvider.getValue(
      'profile',
      'language',
    )) as keyof SCTranslations<SCLanguage>;
    this.translator = new SCThingTranslator(this.language);
    this.menu = await this.navigationService.getMenu();
  }
}
