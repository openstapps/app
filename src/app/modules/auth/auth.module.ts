import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {Platform} from '@ionic/angular';
import {Requestor, StorageBackend} from '@openid/appauth';
import {storageFactory} from './factories';
import {Browser} from 'ionic-appauth';
import {CapacitorBrowser} from 'ionic-appauth/lib/capacitor';
import {httpFactory} from './factories/http.factory';
import {HttpClient} from '@angular/common/http';
import {AuthRoutingModule} from './auth-routing.module';
import {TranslateModule} from '@ngx-translate/core';
import {AuthCallbackPageComponent} from './auth-callback/page/auth-callback-page.component';
import {PAIAAuthCallbackPageComponent} from './paia/auth-callback/page/paiaauth-callback-page.component';
import {DefaultAuthService} from './default-auth.service';
import {PAIAAuthService} from './paia/paia-auth.service';

@NgModule({
  declarations: [AuthCallbackPageComponent, PAIAAuthCallbackPageComponent],
  imports: [CommonModule, AuthRoutingModule, TranslateModule],
  providers: [
    {
      provide: StorageBackend,
      useFactory: storageFactory,
      deps: [Platform],
    },
    {
      provide: Requestor,
      useFactory: httpFactory,
      deps: [Platform, HttpClient],
    },
    {
      provide: Browser,
      useClass: CapacitorBrowser,
    },
    DefaultAuthService,
    PAIAAuthService,
  ],
})
export class AuthModule {}
