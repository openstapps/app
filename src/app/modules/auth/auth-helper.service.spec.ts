/*
 * Copyright (C) 2023 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {TestBed} from '@angular/core/testing';
import {AuthHelperService} from './auth-helper.service';
import {ConfigProvider} from '../config/config.provider';
import {StorageProvider} from '../storage/storage.provider';
import {DefaultAuthService} from './default-auth.service';
import {Browser} from 'ionic-appauth';
import {Requestor, StorageBackend} from '@openid/appauth';
import {TranslateService} from '@ngx-translate/core';
import {PAIAAuthService} from './paia/paia-auth.service';
import {LoggerConfig, LoggerModule, NGXLogger} from 'ngx-logger';
import {StAppsWebHttpClient} from '../data/stapps-web-http-client.provider';
import {HttpClientModule} from '@angular/common/http';
import {SimpleBrowser} from '../../util/browser.factory';

describe('AuthHelperService', () => {
  let authHelperService: AuthHelperService;
  const storageProviderSpy = jasmine.createSpyObj('StorageProvider', ['init', 'get', 'has', 'put', 'search']);
  const translateServiceSpy = jasmine.createSpyObj('TranslateService', ['setDefaultLang', 'use']);
  const defaultAuthServiceMock = jasmine.createSpyObj('DefaultAuthService', ['init', 'setupConfiguration']);
  const paiaAuthServiceMock = jasmine.createSpyObj('PAIAAuthService', ['init', 'setupConfiguration']);
  const authHelperServiceMock = jasmine.createSpyObj('AuthHelperService', ['constructor']);
  const simpleBrowserMock = jasmine.createSpyObj('SimpleBrowser', ['open']);
  const configProvider = jasmine.createSpyObj('ConfigProvider', {
    getAnyValue: {
      default: {
        endpoints: {
          mapping: {
            id: '$.id',
            email: '$.attributes.mailPrimaryAddress',
            givenName: '$.attributes.givenName',
            familyName: '$.attributes.sn',
            name: '$.attributes.sn',
            role: '$.attributes.eduPersonPrimaryAffiliation',
            studentId: '$.attributes.employeeNumber',
          },
        },
      },
    },
  });

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientModule, LoggerModule],
      providers: [
        NGXLogger,
        StAppsWebHttpClient,
        LoggerConfig,
        {
          provide: TranslateService,
          useValue: translateServiceSpy,
        },
        {
          provide: StorageProvider,
          useValue: storageProviderSpy,
        },
        {
          provider: DefaultAuthService,
          useValue: defaultAuthServiceMock,
        },
        {
          provider: PAIAAuthService,
          useValue: paiaAuthServiceMock,
        },
        {
          provide: ConfigProvider,
          useValue: configProvider,
        },
        Browser,
        StorageBackend,
        Requestor,
        {
          provider: AuthHelperService,
          useValue: authHelperServiceMock,
        },
        {
          provide: SimpleBrowser,
          useValue: simpleBrowserMock,
        },
      ],
    });
    authHelperService = TestBed.inject(AuthHelperService);
  });

  describe('getUserFromUserInfo', () => {
    it('should provide user configuration from userInfo', async () => {
      const userConfiguration = authHelperService.getUserFromUserInfo({
        attributes: {
          eduPersonPrimaryAffiliation: 'student',
          employeeNumber: '123456',
          givenName: 'Erika',
          mailPrimaryAddress: 'emuster@anyschool.de',
          oauthClientId: '123-abc-123',
          sn: 'Musterfrau',
          uid: 'emuster',
        },
        id: 'emuster',
        client_id: '123-abc-123',
      });

      expect(userConfiguration).toEqual({
        id: 'emuster',
        givenName: 'Erika',
        familyName: 'Musterfrau',
        name: 'Erika Musterfrau',
        email: 'emuster@anyschool.de',
        role: 'student',
        studentId: '123456',
      });
    });
  });
});
