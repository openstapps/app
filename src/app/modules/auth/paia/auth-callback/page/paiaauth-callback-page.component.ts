/*
 * Copyright (C) 2022 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */

import {Component} from '@angular/core';
import {AuthCallbackPageComponent} from '../../../auth-callback/page/auth-callback-page.component';
import {SCAuthorizationProviderType} from '@openstapps/core';
import {NavController} from '@ionic/angular';
import {Router} from '@angular/router';
import {AuthHelperService} from '../../../auth-helper.service';

@Component({
  templateUrl: '../../../auth-callback/page/auth-callback-page.component.html',
  styleUrls: ['../../../auth-callback/page/auth-callback-page.component.scss'],
})
export class PAIAAuthCallbackPageComponent extends AuthCallbackPageComponent {
  PROVIDER_TYPE = 'paia' as SCAuthorizationProviderType;

  constructor(navCtrl: NavController, router: Router, authHelper: AuthHelperService) {
    super(navCtrl, router, authHelper);
  }
}
