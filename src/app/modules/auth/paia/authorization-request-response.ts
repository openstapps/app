/*
 * Copyright (C) 2021 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */

import {AuthorizationError, AuthorizationRequest} from '@openid/appauth';
import {PAIAAuthorizationResponse} from './paia-authorization-response';

/**
 * Represents a structural type holding both authorization request and response.
 */
export interface PAIAAuthorizationRequestResponse {
  request: AuthorizationRequest;
  response: PAIAAuthorizationResponse | null;
  error: AuthorizationError | null;
}
