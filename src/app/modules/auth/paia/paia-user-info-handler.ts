/*
 * Copyright (C) 2022 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {AuthorizationServiceConfiguration, Requestor} from '@openid/appauth';
import {PAIATokenResponse} from './paia-token-response';

export interface UserInfoHandler {
  performUserInfoRequest(
    configuration: AuthorizationServiceConfiguration,
    token: PAIATokenResponse,
  ): Promise<unknown>;
}

export class PAIAUserInfoHandler implements UserInfoHandler {
  constructor(private requestor: Requestor) {}

  public async performUserInfoRequest(
    configuration: AuthorizationServiceConfiguration,
    token: PAIATokenResponse,
  ): Promise<unknown> {
    const settings: JQueryAjaxSettings = {
      url: `${configuration.userInfoEndpoint}/${token.patron}`,
      method: 'GET',
      headers: {
        Authorization: `${token.tokenType == 'bearer' ? 'Bearer' : token.tokenType} ${token.accessToken}`,
      },
    };

    return this.requestor.xhr(settings);
  }
}
