/*
 * Copyright (C) 2022 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {Injectable} from '@angular/core';
import {
  SCCatalogWithoutReferences,
  SCSearchFilter,
  SCSearchResponse,
  SCSemester,
  SCThingType,
} from '@openstapps/core';
import {DataProvider} from '../data/data.provider';
/**
 * Service for providing catalog and semester data
 */
@Injectable({
  providedIn: 'root',
})
export class CatalogProvider {
  constructor(private readonly dataProvider: DataProvider) {}

  /**
   * Get news messages
   * TODO: make dates sortable on the backend side and then adjust this method
   *
   * @param offset TODO
   * @param superCatalog TODO
   * @param semesterUID TODO
   */
  async getCatalogsWith(
    offset?: number,
    superCatalog?: SCCatalogWithoutReferences,
    semesterUID?: string,
  ): Promise<SCSearchResponse> {
    const filters: SCSearchFilter[] = [
      {
        type: 'value',
        arguments: {
          field: 'type',
          value: 'catalog',
        },
      },
    ];

    if (typeof semesterUID === 'string') {
      filters.push({
        type: 'value',
        arguments: {
          field: 'academicTerm.uid',
          value: semesterUID,
        },
      });
    }

    if (typeof superCatalog?.uid === 'string') {
      filters.push({
        type: 'value',
        arguments: {
          field: 'superCatalog.uid',
          value: superCatalog.uid,
        },
      });
    } else {
      filters.push({
        type: 'value',
        arguments: {
          field: 'level',
          value: '0',
        },
      });
    }

    return this.dataProvider.search({
      filter: {
        arguments: {
          operation: 'and',
          filters: filters,
        },
        type: 'boolean',
      },
      size: 40,
      from: offset,
      sort: [
        {
          type: 'ducet',
          order: 'asc',
          arguments: {
            field: 'name',
          },
        },
      ],
    });
  }

  async getRelevantSemesters(): Promise<SCSemester[]> {
    const response = await this.dataProvider.search({
      filter: {
        arguments: {
          operation: 'and',
          filters: [
            {
              type: 'value',
              arguments: {
                field: 'type',
                value: SCThingType.Semester,
              },
            },
            {
              type: 'date range',
              arguments: {
                bounds: {
                  lowerBound: {
                    limit: `${new Date(new Date().setFullYear(new Date().getFullYear() - 1)).toISOString()}`,
                    mode: 'inclusive',
                  },
                  upperBound: {
                    limit: `${new Date(new Date().setFullYear(new Date().getFullYear() + 1)).toISOString()}`,
                    mode: 'inclusive',
                  },
                },
                field: 'startDate',
              },
            },
          ],
        },
        type: 'boolean',
      },
      sort: [
        {
          arguments: {
            field: 'startDate',
          },
          order: 'desc',
          type: 'generic',
        },
      ],
    });

    return response.data as SCSemester[];
  }
}
