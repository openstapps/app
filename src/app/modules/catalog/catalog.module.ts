/*
 * Copyright (C) 2022 StApps
 *  This program is free software: you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the Free
 *  Software Foundation, version 3.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 *  more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {RouterModule, Routes} from '@angular/router';
import {IonicModule} from '@ionic/angular';
import {TranslateModule} from '@ngx-translate/core';
import {MomentModule} from 'ngx-moment';
import {DataModule} from '../data/data.module';
import {SettingsProvider} from '../settings/settings.provider';
import {CatalogComponent} from './catalog.component';
import {UtilModule} from '../../util/util.module';
import {IonIconModule} from '../../util/ion-icon/ion-icon.module';

const catalogRoutes: Routes = [
  {path: 'catalog', component: CatalogComponent},
  {path: 'catalog/:uid', component: CatalogComponent},
];

/**
 * Catalog Module
 */
@NgModule({
  declarations: [CatalogComponent],
  imports: [
    IonicModule.forRoot(),
    FormsModule,
    TranslateModule.forChild(),
    RouterModule.forChild(catalogRoutes),
    IonIconModule,
    CommonModule,
    MomentModule,
    DataModule,
    UtilModule,
  ],
  providers: [SettingsProvider],
})
export class CatalogModule {}
