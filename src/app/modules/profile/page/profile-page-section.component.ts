/*
 * Copyright (C) 2023 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */

import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {SCSection} from '../../../../config/profile-page-sections';
import {AuthHelperService} from '../../auth/auth-helper.service';
import {Observable, Subscription} from 'rxjs';
import {SCAuthorizationProviderType} from '@openstapps/core';
import Swiper from 'swiper';

@Component({
  selector: 'stapps-profile-page-section',
  templateUrl: 'profile-page-section.html',
  styleUrls: ['profile-page-section.scss'],
})
export class ProfilePageSectionComponent implements OnInit, OnDestroy {
  @Input() item: SCSection;

  @Input() minSlideWidth = 110;

  isLoggedIn: boolean;

  isEnd = false;

  isBeginning = true;

  subscriptions: Subscription[] = [];

  slidesPerView: number;

  slidesFillScreen = false;

  data: {
    [key in SCAuthorizationProviderType]: {loggedIn$: Observable<boolean>};
  } = {
    default: {
      loggedIn$: this.authHelper.getProvider('default').isAuthenticated$,
    },
    paia: {
      loggedIn$: this.authHelper.getProvider('paia').isAuthenticated$,
    },
  };

  constructor(private authHelper: AuthHelperService) {}

  ngOnInit() {
    if (this.item.authProvider) {
      this.subscriptions.push(
        this.data[this.item.authProvider].loggedIn$.subscribe(loggedIn => {
          this.isLoggedIn = loggedIn;
        }),
      );
    }
  }

  activeIndexChange(swiper: Swiper) {
    this.isBeginning = swiper.isBeginning;
    this.isEnd = swiper.isEnd;
    this.slidesFillScreen = this.slidesPerView >= swiper.slides.length;
  }

  resizeSwiper(resizeEvent: ResizeObserverEntry, swiper: Swiper) {
    const slidesPerView =
      Math.floor((resizeEvent.contentRect.width - this.minSlideWidth / 2) / this.minSlideWidth) + 0.5;

    if (slidesPerView > 1 && slidesPerView !== this.slidesPerView) {
      this.slidesPerView = slidesPerView;
      swiper.params.slidesPerView = this.slidesPerView;
      swiper.update();
      this.activeIndexChange(swiper);
    }
  }

  async toggleLogIn() {
    if (!this.item.authProvider) return;
    await (this.isLoggedIn ? this.signOut(this.item.authProvider) : this.signIn(this.item.authProvider));
  }

  async signIn(providerType: SCAuthorizationProviderType) {
    await this.authHelper.getProvider(providerType).signIn();
  }

  async signOut(providerType: SCAuthorizationProviderType) {
    await this.authHelper.getProvider(providerType).signOut();
    await this.authHelper.endBrowserSession(providerType);
  }

  ngOnDestroy() {
    for (const subscription of this.subscriptions) {
      subscription.unsubscribe();
    }
  }
}
