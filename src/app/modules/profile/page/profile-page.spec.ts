/*
 * Copyright (C) 2022 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */

import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {ComponentFixture, TestBed} from '@angular/core/testing';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {RouterTestingModule} from '@angular/router/testing';
import {AuthModule} from '../../auth/auth.module';
import {ProfilePageComponent} from './profile-page.component';
import {TranslateModule} from '@ngx-translate/core';
import {ConfigProvider} from '../../config/config.provider';
import {sampleAuthConfiguration} from '../../../_helpers/data/sample-configuration';
import {StorageProvider} from '../../storage/storage.provider';
import {ScheduleProvider} from '../../calendar/schedule.provider';
import {DataProvider} from '../../data/data.provider';
import {StAppsWebHttpClient} from '../../data/stapps-web-http-client.provider';
import {SimpleBrowser} from '../../../util/browser.factory';

describe('ProfilePage', () => {
  let component: ProfilePageComponent;
  let fixture: ComponentFixture<ProfilePageComponent>;
  let configProvider: ConfigProvider;
  let storageProvider: jasmine.SpyObj<StorageProvider>;
  let simpleBrowser: jasmine.SpyObj<SimpleBrowser>;

  beforeEach(() => {
    configProvider = jasmine.createSpyObj('ConfigProvider', ['init', 'getAnyValue']);
    storageProvider = jasmine.createSpyObj('StorageProvider', ['init', 'get', 'has', 'put']);
    simpleBrowser = jasmine.createSpyObj('SimpleBrowser', ['open']);
    configProvider.getAnyValue = jasmine.createSpy().and.callFake(function () {
      return sampleAuthConfiguration;
    });

    const webHttpClientMethodSpy = jasmine.createSpyObj('StAppsWebHttpClient', ['request']);

    TestBed.configureTestingModule({
      declarations: [ProfilePageComponent],
      imports: [HttpClientTestingModule, RouterTestingModule, AuthModule, TranslateModule.forRoot()],
      providers: [
        {provide: ConfigProvider, useValue: configProvider},
        {provide: StorageProvider, useValue: storageProvider},
        {provide: StAppsWebHttpClient, useValue: webHttpClientMethodSpy},
        {provide: SimpleBrowser, useValue: simpleBrowser},
        ScheduleProvider,
        DataProvider,
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfilePageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
