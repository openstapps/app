/*
 * Copyright (C) 2022 StApps
 *  This program is free software: you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the Free
 *  Software Foundation, version 3.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 *  more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  this program. If not, see <https://www.gnu.org/licenses/>.
 */

import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {IonicModule} from '@ionic/angular';
import {RouterModule, Routes} from '@angular/router';
import {TranslateModule} from '@ngx-translate/core';
import {LibraryAccountPageComponent} from './account/account.page';
import {ProfilePageComponent} from './account/profile/profile-page.component';
import {CheckedOutPageComponent} from './account/checked-out/checked-out-page.component';
import {HoldsPageComponent} from './account/holds/holds-page.component';
import {FinesPageComponent} from './account/fines/fines-page.component';
import {PAIAItemComponent} from './account/elements/paia-item/paiaitem.component';
import {FirstLastNamePipe} from './account/first-last-name.pipe';
import {AuthGuardService} from '../auth/auth-guard.service';
import {ProtectedRoutes} from '../auth/protected.routes';
import {MomentModule} from 'ngx-moment';
import {FeeItemComponent} from './account/elements/fee-item/fee-item.component';
import {DataModule} from '../data/data.module';
import {UtilModule} from '../../util/util.module';
import {IonIconModule} from '../../util/ion-icon/ion-icon.module';

const routes: ProtectedRoutes | Routes = [
  {
    path: 'library-account',
    component: LibraryAccountPageComponent,
    data: {authProvider: 'paia'},
    canActivate: [AuthGuardService],
  },
  {
    path: 'library-account/profile',
    component: ProfilePageComponent,
    data: {authProvider: 'paia'},
    canActivate: [AuthGuardService],
  },
  {
    path: 'library-account/checked-out',
    component: CheckedOutPageComponent,
    data: {authProvider: 'paia'},
    canActivate: [AuthGuardService],
  },
  {
    path: 'library-account/holds',
    component: HoldsPageComponent,
    data: {authProvider: 'paia'},
    canActivate: [AuthGuardService],
  },
  {
    path: 'library-account/fines',
    component: FinesPageComponent,
    data: {authProvider: 'paia'},
    canActivate: [AuthGuardService],
  },
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    IonIconModule,
    RouterModule.forChild(routes),
    TranslateModule,
    MomentModule,
    DataModule,
    UtilModule,
  ],
  declarations: [
    LibraryAccountPageComponent,
    ProfilePageComponent,
    CheckedOutPageComponent,
    HoldsPageComponent,
    FinesPageComponent,
    PAIAItemComponent,
    FirstLastNamePipe,
    FeeItemComponent,
  ],
})
export class LibraryModule {}
