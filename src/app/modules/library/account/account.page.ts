import {Component} from '@angular/core';
import {LibraryAccountService} from './library-account.service';

@Component({
  templateUrl: './account.page.html',
  styleUrls: ['./account.page.scss'],
})
export class LibraryAccountPageComponent {
  name?: string;

  constructor(private readonly libraryAccountService: LibraryAccountService) {}

  async ionViewWillEnter(): Promise<void> {
    const patron = await this.libraryAccountService.getProfile();
    this.name = patron?.name;
  }
}
