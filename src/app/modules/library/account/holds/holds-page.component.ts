/*
 * Copyright (C) 2023 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */

import {Component} from '@angular/core';
import {DocumentAction, PAIADocument, PAIADocumentStatus} from '../../types';
import {LibraryAccountService} from '../library-account.service';

type Segment = 'orders' | 'reservations';

@Component({
  selector: 'stapps-holds',
  templateUrl: './holds-page.html',
  styleUrls: ['./holds-page.scss'],
})
export class HoldsPageComponent {
  paiaDocuments?: PAIADocument[];

  paiaDocumentStatus = PAIADocumentStatus;

  activeSegment: Segment = 'orders';

  constructor(private readonly libraryAccountService: LibraryAccountService) {}

  async ionViewWillEnter(): Promise<void> {
    await this.fetchItems(this.activeSegment);
  }

  async fetchItems(segment: Segment) {
    this.activeSegment = segment;
    this.paiaDocuments = undefined;
    const itemsStatus =
      segment === 'reservations'
        ? [PAIADocumentStatus.Reserved]
        : [PAIADocumentStatus.Ordered, PAIADocumentStatus.Provided];
    try {
      this.paiaDocuments = await this.libraryAccountService.getFilteredItems(itemsStatus);
    } catch {
      await this.libraryAccountService.handleError();
      this.paiaDocuments = [];
    }
  }

  toNumber = Number;

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  async segmentChanged(event: any) {
    await this.fetchItems(event.detail.value);
  }

  async onDocumentAction(documentAction: DocumentAction) {
    const answer = await this.libraryAccountService.handleDocumentAction(documentAction);

    if (answer) await this.fetchItems(this.activeSegment);
  }
}
