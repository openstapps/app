/*
 * Copyright (C) 2022 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */

import {Component, EventEmitter, Input, Output} from '@angular/core';
import {DocumentAction, PAIADocument} from '../../../types';

@Component({
  selector: 'stapps-paia-item',
  templateUrl: './paiaitem.html',
  styleUrls: ['./paiaitem.scss'],
})
export class PAIAItemComponent {
  @Input() item: PAIADocument;

  @Input()
  propertiesToShow: (keyof PAIADocument)[];

  @Input()
  listName: string;

  @Output()
  documentAction: EventEmitter<DocumentAction> = new EventEmitter<DocumentAction>();

  async onClick(action: DocumentAction['action']) {
    this.documentAction.emit({doc: this.item, action});
  }
}
