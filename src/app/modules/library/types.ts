/*
 * Copyright (C) 2022 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */

export interface PAIAPatron {
  id: string;
  name: string;
  email?: string;
  address?: string;
  expires?: string;
  status?: string;
  type?: string;
  note?: string;
}

/*
 * Document representing a library item received from the HeBIS PAIA service
 * TODO: would be good to standardize the items of HeBIS PAIA to match the official PAIA documentation
 *  e.g. status should be number (0-5), and queue, renewals, reminder should be numbers too
 *  https://gbv.github.io/paia/paia.html#documents
 */
export interface PAIADocument {
  status: string;
  item?: string;
  edition?: string;
  about?: string;
  label?: string;
  queue?: string;
  renewals?: string;
  reminder?: string;
  endtime?: string;
  duedate?: string;
  cancancel?: boolean;
  canrenew?: boolean;
  storage?: string;
  // with locations
  condition?: unknown;
}

export interface PAIAItems {
  doc: PAIADocument[];
}

export interface PAIAFee {
  amount: string;
  date?: string;
  about?: string;
  item?: string;
  edition?: string;
  feetype?: string;
  feeid?: string;
}

export interface PAIAFees {
  amount?: string;
  fee: PAIAFee[];
}

export enum PAIADocumentStatus {
  NoRelation = 0,
  Reserved = 1,
  Ordered = 2,
  Held = 3,
  Provided = 4,
  Rejected = 5,
}

export interface DocumentAction {
  action: 'cancel' | 'renew';
  doc: PAIADocument;
}
