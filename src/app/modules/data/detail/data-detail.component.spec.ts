/*
 * Copyright (C) 2023 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */

/* eslint-disable @typescript-eslint/no-non-null-assertion,@typescript-eslint/no-explicit-any */
import {CUSTOM_ELEMENTS_SCHEMA, DebugElement} from '@angular/core';
import {ComponentFixture, TestBed} from '@angular/core/testing';
import {ActivatedRoute, RouterModule} from '@angular/router';
import {IonTitle} from '@ionic/angular';
import {TranslateLoader, TranslateModule, TranslateService} from '@ngx-translate/core';
import {sampleThingsMap} from '../../../_helpers/data/sample-things';
import {DataRoutingModule} from '../data-routing.module';
import {DataModule} from '../data.module';
import {DataProvider} from '../data.provider';
import {DataDetailComponent} from './data-detail.component';
import {By} from '@angular/platform-browser';
import {Observable, of} from 'rxjs';
import {StorageProvider} from '../../storage/storage.provider';
import {LoggerModule, NgxLoggerLevel} from 'ngx-logger';

const translations: any = {data: {detail: {TITLE: 'Foo'}}};

class TranslateFakeLoader implements TranslateLoader {
  getTranslation(_lang: string): Observable<any> {
    return of(translations);
  }
}

describe('DataDetailComponent', () => {
  let comp: DataDetailComponent;
  let fixture: ComponentFixture<DataDetailComponent>;
  let detailPage: DebugElement;
  let dataProvider: DataProvider;
  const sampleThing = sampleThingsMap.message[0];
  let translateService: TranslateService;

  // @Component({ selector: 'stapps-data-list-item', template: '' })
  // class DataListItemComponent {
  //   @Input() item;
  // }

  const fakeActivatedRoute = {
    snapshot: {
      paramMap: {
        get: () => {
          return sampleThing.uid;
        },
      },
    },
  };

  const storageProviderSpy = jasmine.createSpyObj('StorageProvider', ['init', 'get', 'has', 'put', 'search']);

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterModule.forRoot([], {relativeLinkResolution: 'legacy'}),
        DataRoutingModule,
        DataModule,
        TranslateModule.forRoot({
          loader: {provide: TranslateLoader, useClass: TranslateFakeLoader},
        }),
        LoggerModule.forRoot({level: NgxLoggerLevel.TRACE}),
      ],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: fakeActivatedRoute,
        },
        {
          provide: StorageProvider,
          useValue: storageProviderSpy,
        },
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    }).compileComponents();
  });

  beforeEach(() => {
    dataProvider = TestBed.inject(DataProvider);
    translateService = TestBed.inject(TranslateService);
    spyOn(dataProvider, 'get' as any).and.returnValue(Promise.resolve(sampleThing));
    spyOn(DataDetailComponent.prototype, 'getItem').and.callThrough();
    fixture = TestBed.createComponent(DataDetailComponent);
    comp = fixture.componentInstance;
    detailPage = fixture.debugElement;
    translateService.use('foo');
    fixture.detectChanges();
  });

  it('should create component', () => expect(comp).toBeDefined());

  it('should have appropriate title', async () => {
    const title: DebugElement = detailPage.query(By.directive(IonTitle));
    expect(title!.nativeElement.textContent).toBe('Foo');
  });

  it('should get a data item', () => {
    comp.getItem(sampleThing.uid, false);
    expect(DataDetailComponent.prototype.getItem).toHaveBeenCalledWith(sampleThing.uid, false);
  });

  it('should get a data item when the view is entered', () => {
    comp.ionViewWillEnter();
    expect(DataDetailComponent.prototype.getItem).toHaveBeenCalledWith(sampleThing.uid, false);
  });
});
