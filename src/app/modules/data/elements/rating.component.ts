/*
 * Copyright (C) 2023 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {Component, ElementRef, HostListener, Input} from '@angular/core';
import {SCDish, SCRatingRequest, SCUuid} from '@openstapps/core';
import {RatingProvider} from '../rating.provider';
import {ratingAnimation} from './rating.animation';

@Component({
  selector: 'stapps-rating',
  templateUrl: 'rating.html',
  styleUrls: ['rating.scss'],
  animations: [ratingAnimation],
})
export class StappsRatingComponent {
  rate = false;

  rated = false;

  canBeRated = false;

  uid: SCUuid;

  rating?: number;

  @Input() set item(value: SCDish) {
    this.uid = value.uid;

    Promise.all([this.ratingProvider.canRate(value), this.ratingProvider.hasRated(this.uid)] as const).then(
      ([canRate, hasRated]) => {
        this.canBeRated = canRate;
        this.rated = hasRated;
      },
    );
  }

  constructor(readonly elementRef: ElementRef, readonly ratingProvider: RatingProvider) {}

  async submitRating(rating: number) {
    this.rating = rating;
    try {
      await this.ratingProvider.rate(this.uid, rating as SCRatingRequest['rating']);
      this.rated = true;
    } catch {
      this.rating = undefined;
      // allow change detection to catch up first
      setTimeout(() => {
        this.rate = false;
      });
    }
  }

  @HostListener('document:mousedown', ['$event'])
  clickOutside(event: MouseEvent) {
    if (this.rating) return;
    if (!this.elementRef.nativeElement.contains(event.target)) {
      this.rate = false;
    }
  }
}
