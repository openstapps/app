/*
 * Copyright (C) 2022 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */

import {Component, Input} from '@angular/core';
import {SimpleBrowser} from '../../../util/browser.factory';

@Component({
  selector: 'stapps-external-link',
  templateUrl: './external-link.html',
  styleUrls: ['./external-link.scss'],
})
export class ExternalLinkComponent {
  @Input() link: string;

  @Input() text: string;

  constructor(private browser: SimpleBrowser) {}

  onLinkClick(url: string) {
    // make sure if the url is valid and then open it in the browser (prevent problem in iOS)
    this.browser.open(new URL(url).href);
  }
}
