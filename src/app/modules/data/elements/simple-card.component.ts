/*
 * Copyright (C) 2022 StApps
 *  This program is free software: you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the Free
 *  Software Foundation, version 3.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 *  more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {Component, Input} from '@angular/core';
import {SCThingWithoutReferences} from '@openstapps/core';

/**
 * TODO
 */
@Component({
  selector: 'stapps-simple-card',
  templateUrl: 'simple-card.html',
})
export class SimpleCardComponent {
  /**
   * TODO
   */
  areThings = false;

  /**
   * TODO
   */
  @Input() content: string | string[] | SCThingWithoutReferences[];

  /**
   * TODO
   */
  @Input() isMarkdown = false;

  /**
   * TODO
   */
  @Input() title: string;

  /**
   * TODO
   */
  // eslint-disable-next-line class-methods-use-this
  isString(data: unknown): data is string {
    return typeof data === 'string';
  }

  /**
   * TODO
   */
  // eslint-disable-next-line class-methods-use-this
  isThing(something: unknown): something is SCThingWithoutReferences {
    // bypass the 'type' field check because of translated values
    return typeof something === 'object';
  }
}
