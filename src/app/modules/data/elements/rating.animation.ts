/*
 * Copyright (C) 2023 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {animate, group, keyframes, query, stagger, style, transition, trigger} from '@angular/animations';

const duration = '800ms';
const timingFunction = 'cubic-bezier(0.16, 1, 0.3, 1)';
const timing = `${duration} ${timingFunction}`;
const staggerTiming = '75ms';

export const ratingAnimation = trigger('rating', [
  transition('rated => void', [
    style({opacity: 1, translate: '0 0'}),
    group([
      query(
        'ion-icon:not(.rated-value)',
        [
          style({translate: '0 0', opacity: 1}),
          stagger(staggerTiming, animate(timing, style({translate: '0 -16px', opacity: 0}))),
        ],
        {optional: true},
      ),
      query(
        '.rated-value',
        [
          style({translate: '0 0', opacity: 1}),
          animate(`${duration} 200ms ${timingFunction}`, style({translate: '0 -16px', opacity: 0})),
        ],
        {optional: true},
      ),
      query(
        '.rated-value ~ .thank-you',
        [
          style({translate: '0 16px', opacity: 0}),
          animate(`${duration} 500ms ${timingFunction}`, style({translate: '0 0', opacity: 1})),
          animate('1000ms', style({opacity: 1})),
        ],
        {optional: true},
      ),
    ]),
    animate(timing, style({opacity: 0, translate: '8px 0'})),
  ]),
  transition('abandoned => void', [
    style({opacity: 1, translate: '0 0'}),
    animate(timing, style({opacity: 0, translate: '8px 0'})),
  ]),
  transition(':enter', [
    style({
      translate: '40% 0',
      background: 'rgba(var(--ion-color-light), 0)',
    }),
    group([
      animate(
        timing,
        style({
          translate: '0 0',
          background: 'rgba(var(--ion-color-light-rgb), 1)',
        }),
      ),
      query('ion-icon', [
        style({
          translate: '16px 0',
          scale: '0.4 0.2',
          opacity: 0,
          offset: 0,
          willChange: 'scale, translate',
        }),
        stagger(staggerTiming, [
          animate(
            timing,
            keyframes([
              style({
                scale: 1.3,
                offset: 0.2,
              }),
              style({
                translate: '0 0',
                scale: '1 1',
                opacity: 1,
                offset: 1,
              }),
            ]),
          ),
        ]),
      ]),
    ]),
  ]),
]);
