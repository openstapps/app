/*
 * Copyright (C) 2023 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {Component, Input} from '@angular/core';
import {SCAcademicPriceGroup, SCThingThatCanBeOfferedOffer} from '@openstapps/core';
import {SettingsProvider} from '../../settings/settings.provider';

/**
 * TODO
 */
@Component({
  selector: 'stapps-offers-in-list',
  templateUrl: 'offers-in-list.html',
})
export class OffersInListComponent {
  /**
   * TODO
   */
  @Input() set offers(it: Array<SCThingThatCanBeOfferedOffer<SCAcademicPriceGroup>>) {
    this._offers = it;
    this.price = it[0].prices?.default;
    this.settingsProvider.getSetting('profile', 'group').then(group => {
      this.price = it[0].prices?.[(group.value as string).replace(/s$/, '') as never];
    });

    const availabilities = new Set(it.map(offer => offer.availability));
    this.soldOut = availabilities.has('out of stock') && availabilities.size === 1;
  }

  price?: number;

  soldOut: boolean;

  _offers: Array<SCThingThatCanBeOfferedOffer<SCAcademicPriceGroup>>;

  constructor(readonly settingsProvider: SettingsProvider) {}
}
