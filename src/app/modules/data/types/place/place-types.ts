/*
 * Copyright (C) 2021 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {SCBuilding, SCFloor, SCPointOfInterest, SCRoom} from '@openstapps/core';

/**
 * Possible place types
 */
export type PlaceTypes = SCBuilding | SCRoom | SCPointOfInterest | SCFloor;

/**
 * Place types with their dynamic distance (from the position of the device)
 */
export type PlaceTypesWithDistance = PlaceTypes & {
  distance?: number;
};

/**
 * Detects "null island" places, which means places with point coordinates [0, 0]
 *
 * @param place A place to check
 */
export function hasValidLocation(place: Exclude<PlaceTypes, SCFloor>) {
  return place.geo.point.coordinates.some(coordinate => coordinate !== 0);
}

/**
 * Provide information if a place is a floor
 *
 * @param place A place to check
 */
export function isSCFloor(place: PlaceTypes): place is SCFloor {
  return place.type === 'floor';
}
