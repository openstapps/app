/*
 * Copyright (C) 2023 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {Component, Input} from '@angular/core';
import {PositionService} from '../../../map/position.service';
import {interval, Subscription} from 'rxjs';
import {hasValidLocation, isSCFloor, PlaceTypes, PlaceTypesWithDistance} from './place-types';

/**
 * Shows a place as a list item
 */
@Component({
  selector: 'stapps-place-list-item',
  templateUrl: 'place-list-item.html',
  styleUrls: ['place-list-item.scss'],
})
export class PlaceListItemComponent {
  /**
   * Item getter
   */
  get item(): PlaceTypesWithDistance {
    return this._item;
  }

  /**
   * An item to show (setter is used as there were issues assigning the distance to the right place in a list)
   */
  @Input() set item(item: PlaceTypes) {
    this._item = item;
    if (!isSCFloor(item) && hasValidLocation(item)) {
      this.distance = this.positionService.getDistance(item.geo.point);
      this.distanceSubscription = interval(10_000).subscribe(_ => {
        this.distance = this.positionService.getDistance(item.geo.point);
      });
    }
  }

  /**
   * An item to show
   */
  private _item: PlaceTypesWithDistance;

  /**
   * Distance in meters
   */
  distance?: number;

  distanceSubscription?: Subscription;

  constructor(private positionService: PositionService) {}
}
