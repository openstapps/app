/*
 * Copyright (C) 2022 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {Component, Input, OnInit} from '@angular/core';
import {SCCatalog, SCSearchBooleanFilter, SCDucetSort} from '@openstapps/core';
import {SearchPageComponent} from '../../list/search-page.component';

@Component({
  selector: 'stapps-catalog-detail-content',
  templateUrl: 'catalog-detail-content.html',
  styleUrls: ['catalog-detail-content.scss'],
})
export class CatalogDetailContentComponent extends SearchPageComponent implements OnInit {
  /**
   * SCCatalog to display
   */
  @Input() item: SCCatalog;

  ngOnInit() {
    super.ngOnInit();
  }

  initialize() {
    this.showDefaultData = true;
    this.pageSize = 100;

    const nameSort: SCDucetSort = {
      arguments: {field: 'name'},
      order: 'asc',
      type: 'ducet',
    };

    const typeSort: SCDucetSort = {
      arguments: {field: 'type'},
      order: 'desc',
      type: 'ducet',
    };

    this.sortQuery = [typeSort, nameSort];

    const subCatalogFilter: SCSearchBooleanFilter = {
      arguments: {
        operation: 'and',
        filters: [
          {
            type: 'value',
            arguments: {
              field: 'type',
              value: 'catalog',
            },
          },
          {
            type: 'value',
            arguments: {
              field: 'superCatalog.uid',
              value: this.item.uid,
            },
          },
        ],
      },
      type: 'boolean',
    };

    const subEventsFilter: SCSearchBooleanFilter = {
      arguments: {
        operation: 'and',
        filters: [
          {
            type: 'value',
            arguments: {
              field: 'type',
              value: 'academic event',
            },
          },
          {
            type: 'value',
            arguments: {
              field: 'catalogs.uid',
              value: this.item.uid,
            },
          },
        ],
      },
      type: 'boolean',
    };

    this.forcedFilter = {
      arguments: {
        filters: [subCatalogFilter, subEventsFilter],
        operation: 'or',
      },
      type: 'boolean',
    };
  }
}
