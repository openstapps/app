/*
 * Copyright (C) 2019 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {Component, Input} from '@angular/core';
import {SCMessage} from '@openstapps/core';
import {SimpleBrowser} from '../../../../util/browser.factory';

/**
 * TODO
 */
@Component({
  selector: 'stapps-message-detail-content',
  templateUrl: 'message-detail-content.html',
  styleUrls: ['message-detail-content.scss'],
})
export class MessageDetailContentComponent {
  constructor(private browser: SimpleBrowser) {}

  /**
   * TODO
   */
  @Input() item: SCMessage;

  /**
   * Open the external link when clicked
   *
   * @param url Web address to open
   */
  onLinkClick(url: string) {
    // make sure if the url is valid and then open it in the browser (prevent problem in iOS)
    this.browser.open(new URL(url).href);
  }
}
