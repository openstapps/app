/*
 * Copyright (C) 2023 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {ScrollingModule} from '@angular/cdk/scrolling';
import {CommonModule} from '@angular/common';
import {HttpClientModule} from '@angular/common/http';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {IonicModule, Platform} from '@ionic/angular';
import {TranslateModule} from '@ngx-translate/core';
import {MarkdownModule} from 'ngx-markdown';
import {MomentModule} from 'ngx-moment';
import {ThingTranslateModule} from '../../translation/thing-translate.module';
import {MenuModule} from '../menu/menu.module';
import {ScheduleProvider} from '../calendar/schedule.provider';
import {StorageModule} from '../storage/storage.module';
import {ActionChipListComponent} from './chips/action-chip-list.component';
import {EditEventSelectionComponent} from './chips/edit-event-selection.component';
import {AddEventActionChipComponent} from './chips/data/add-event-action-chip.component';
import {LocateActionChipComponent} from './chips/data/locate-action-chip.component';
import {DataFacetsProvider} from './data-facets.provider';
import {DataIconPipe} from './data-icon.pipe';
import {DataRoutingModule} from './data-routing.module';
import {DataProvider} from './data.provider';
import {DataDetailContentComponent} from './detail/data-detail-content.component';
import {DataDetailComponent} from './detail/data-detail.component';
import {AddressDetailComponent} from './elements/address-detail.component';
import {OffersDetailComponent} from './elements/offers-detail.component';
import {OffersInListComponent} from './elements/offers-in-list.component';
import {OriginDetailComponent} from './elements/origin-detail.component';
import {SimpleCardComponent} from './elements/simple-card.component';
import {DataListComponent} from './list/data-list.component';
import {FoodDataListComponent} from './list/food-data-list.component';
import {SearchPageComponent} from './list/search-page.component';
import {StAppsWebHttpClient} from './stapps-web-http-client.provider';
import {CatalogDetailContentComponent} from './types/catalog/catalog-detail-content.component';
import {DateSeriesDetailContentComponent} from './types/date-series/date-series-detail-content.component';
import {DishDetailContentComponent} from './types/dish/dish-detail-content.component';
import {EventDetailContentComponent} from './types/event/event-detail-content.component';
import {EventListItemComponent} from './types/event/event-list-item.component';
import {FavoriteDetailContentComponent} from './types/favorite/favorite-detail-content.component';
import {MessageDetailContentComponent} from './types/message/message-detail-content.component';
import {OrganizationDetailContentComponent} from './types/organization/organization-detail-content.component';
import {PersonDetailContentComponent} from './types/person/person-detail-content.component';
import {PlaceDetailContentComponent} from './types/place/place-detail-content.component';
import {PlaceListItemComponent} from './types/place/place-list-item.component';
import {PlaceMensaDetailComponent} from './types/place/special/mensa/place-mensa-detail.component';
import {SemesterDetailContentComponent} from './types/semester/semester-detail-content.component';
import {MapWidgetComponent} from '../map/widget/map-widget.component';
import {LeafletModule} from '@asymmetrik/ngx-leaflet';
import {SkeletonSimpleCardComponent} from './elements/skeleton-simple-card.component';
import {CatalogListItemComponent} from './types/catalog/catalog-list-item.component';
import {DataListItemComponent} from './list/data-list-item.component';
import {DateSeriesListItemComponent} from './types/date-series/date-series-list-item.component';
import {DishListItemComponent} from './types/dish/dish-list-item.component';
import {FavoriteListItemComponent} from './types/favorite/favorite-list-item.component';
import {LongInlineTextComponent} from './elements/long-inline-text.component';
import {MessageListItemComponent} from './types/message/message-list-item.component';
import {OrganizationListItemComponent} from './types/organization/organization-list-item.component';
import {PersonListItemComponent} from './types/person/person-list-item.component';
import {SkeletonListItemComponent} from './elements/skeleton-list-item.component';
import {SkeletonSegmentComponent} from './elements/skeleton-segment-button.component';
import {VideoDetailContentComponent} from './types/video/video-detail-content.component';
import {SemesterListItemComponent} from './types/semester/semester-list-item.component';
import {VideoListItemComponent} from './types/video/video-list-item.component';
import {OriginInListComponent} from './elements/origin-in-list.component';
import {CoordinatedSearchProvider} from './coordinated-search.provider';
import {FavoriteButtonComponent} from './elements/favorite-button.component';
import {SimpleDataListComponent} from './list/simple-data-list.component';
import {TitleCardComponent} from './elements/title-card.component';
import {CalendarService} from '../calendar/calendar.service';
import {RoutingStackService} from '../../util/routing-stack.service';
import {DataPathComponent} from './detail/data-path.component';
import {EventRoutePathComponent} from './types/event/event-route-path.component';
import {UtilModule} from '../../util/util.module';
import {TreeListComponent} from './list/tree-list.component';
import {TreeListFragmentComponent} from './list/tree-list-fragment.component';
import {SettingsProvider} from '../settings/settings.provider';
import {IonIconModule} from '../../util/ion-icon/ion-icon.module';
import {ExternalLinkComponent} from './elements/external-link.component';
import {ArticleListItemComponent} from './types/article/article-item.component';
import {ArticleContentComponent} from './types/article/article-content.component';
import {BookDetailContentComponent} from './types/book/book-detail-content.component';
import {BookListItemComponent} from './types/book/book-list-item.component';
import {PeriodicalListItemComponent} from './types/periodical/periodical-list-item.component';
import {PeriodicalDetailContentComponent} from './types/periodical/periodical-detail-content.component';
import {DataListItemHostDirective} from './list/data-list-item-host.directive';
import {DataListItemHostDefaultComponent} from './list/data-list-item-host-default.component';
import {browserFactory, SimpleBrowser} from '../../util/browser.factory';
import {StappsRatingComponent} from './elements/rating.component';
import {DishCharacteristicsComponent} from './types/dish/dish-characteristics.component';
import {SkeletonListComponent} from './list/skeleton-list.component';
import {CertificationsInDetailComponent} from './elements/certifications-in-detail.component';

/**
 * Module for handling data
 */
@NgModule({
  declarations: [
    ActionChipListComponent,
    AddEventActionChipComponent,
    EditEventSelectionComponent,
    AddressDetailComponent,
    CatalogDetailContentComponent,
    CatalogListItemComponent,
    CertificationsInDetailComponent,
    DishCharacteristicsComponent,
    DataDetailComponent,
    DataDetailContentComponent,
    DataIconPipe,
    DataListComponent,
    DataListItemComponent,
    DataPathComponent,
    EventRoutePathComponent,
    DateSeriesDetailContentComponent,
    DateSeriesListItemComponent,
    DishDetailContentComponent,
    DishListItemComponent,
    EventDetailContentComponent,
    EventListItemComponent,
    FavoriteButtonComponent,
    FavoriteDetailContentComponent,
    SkeletonListComponent,
    FavoriteListItemComponent,
    FoodDataListComponent,
    LocateActionChipComponent,
    LongInlineTextComponent,
    MapWidgetComponent,
    MessageDetailContentComponent,
    MessageListItemComponent,
    OffersDetailComponent,
    OffersInListComponent,
    OrganizationDetailContentComponent,
    OrganizationListItemComponent,
    OriginDetailComponent,
    OriginInListComponent,
    PersonDetailContentComponent,
    PersonListItemComponent,
    PlaceDetailContentComponent,
    PlaceListItemComponent,
    PlaceMensaDetailComponent,
    SearchPageComponent,
    SemesterDetailContentComponent,
    SemesterListItemComponent,
    DataListItemHostDirective,
    DataListItemHostDefaultComponent,
    SimpleCardComponent,
    SkeletonListItemComponent,
    SkeletonSegmentComponent,
    StappsRatingComponent,
    SkeletonSimpleCardComponent,
    TreeListComponent,
    TreeListFragmentComponent,
    VideoDetailContentComponent,
    VideoListItemComponent,
    SimpleDataListComponent,
    TitleCardComponent,
    ExternalLinkComponent,
    ArticleListItemComponent,
    ArticleContentComponent,
    BookListItemComponent,
    BookDetailContentComponent,
    PeriodicalListItemComponent,
    PeriodicalDetailContentComponent,
  ],
  entryComponents: [DataListComponent, SimpleDataListComponent],
  imports: [
    CommonModule,
    DataRoutingModule,
    FormsModule,
    HttpClientModule,
    IonicModule.forRoot(),
    LeafletModule,
    MarkdownModule.forRoot(),
    MenuModule,
    IonIconModule,
    MomentModule.forRoot({
      relativeTimeThresholdOptions: {
        m: 59,
      },
    }),
    ScrollingModule,
    StorageModule,
    TranslateModule.forChild(),
    ThingTranslateModule.forChild(),
    UtilModule,
  ],
  providers: [
    CoordinatedSearchProvider,
    DataProvider,
    DataFacetsProvider,
    Geolocation,
    ScheduleProvider,
    StAppsWebHttpClient,
    CalendarService,
    RoutingStackService,
    SettingsProvider,
    {
      provide: SimpleBrowser,
      useFactory: browserFactory,
      deps: [Platform],
    },
  ],
  exports: [
    DataDetailComponent,
    DataDetailContentComponent,
    DataIconPipe,
    DataListComponent,
    DataListItemComponent,
    DateSeriesListItemComponent,
    PlaceListItemComponent,
    SimpleCardComponent,
    SkeletonListItemComponent,
    SkeletonSimpleCardComponent,
    SearchPageComponent,
    SimpleDataListComponent,
    OriginDetailComponent,
    FavoriteButtonComponent,
    TreeListComponent,
    ExternalLinkComponent,
    ArticleContentComponent,
    BookDetailContentComponent,
    PeriodicalDetailContentComponent,
    TitleCardComponent,
  ],
})
export class DataModule {}
