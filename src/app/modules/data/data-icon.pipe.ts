/*
 * Copyright (C) 2022 StApps
 *  This program is free software: you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the Free
 *  Software Foundation, version 3.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 *  more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {Pipe, PipeTransform} from '@angular/core';
import {SCThingType} from '@openstapps/core';
import {DataIcons} from './data-icon.config';

/**
 * Converts the data type text into the icon name
 */
@Pipe({
  name: 'dataIcon',
})
export class DataIconPipe implements PipeTransform {
  /**
   * Mapping from data types to ionic icons to show
   */
  typeIconMap = DataIcons;

  /**
   * Provide the icon name from the data type
   */
  transform(type: SCThingType): string {
    return this.typeIconMap[type];
  }
}
