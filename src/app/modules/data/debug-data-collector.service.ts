/*
 * Copyright (C) 2021 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {Injectable} from '@angular/core';
import {SCFeedbackRequestMetaData} from '@openstapps/core';
import {Platform} from '@ionic/angular';
import {DataProvider} from './data.provider';
import {NavigationEnd, Router} from '@angular/router';
import {SettingsProvider} from '../settings/settings.provider';

@Injectable({
  providedIn: 'root',
})
export class DebugDataCollectorService {
  /**
   * Previously visited route
   */
  previousRoute: string;

  /**
   * Current route
   */
  currentRoute: string;

  constructor(
    private platform: Platform,
    private dataProvider: DataProvider,
    private router: Router,
    private settingsProvider: SettingsProvider,
  ) {
    this.currentRoute = this.router.url;
    router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        this.previousRoute = this.currentRoute;
        this.currentRoute = event.url;
      }
    });
  }

  /**
   * Provides meta data for a feedback
   */
  async getFeedbackMetaData(): Promise<SCFeedbackRequestMetaData> {
    return {
      debug: false,
      platform: this.platform.platforms().join(','),
      scope: {},
      state: {
        route: this.previousRoute,
        settings: await this.settingsProvider.getCache(),
      },
      userAgent: window.navigator.userAgent,
      version: this.dataProvider.appVersion,
      sendable: true,
    };
  }
}
