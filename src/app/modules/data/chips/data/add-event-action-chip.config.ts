/*
 * Copyright (C) 2022 StApps
 *  This program is free software: you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the Free
 *  Software Foundation, version 3.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 *  more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  this program. If not, see <https://www.gnu.org/licenses/>.
 */

import {SCIcon} from '../../../../util/ion-icon/icon';

export enum AddEventStates {
  ADDED_ALL,
  ADDED_SOME,
  REMOVED_ALL,
  UNAVAILABLE,
}

export const AddEventStatesMap = {
  [AddEventStates.ADDED_ALL]: {
    icon: SCIcon`event_available`,
    fill: true,
    label: 'data.chips.add_events.ADDED_ALL',
    disabled: false,
    color: 'success',
  },
  [AddEventStates.ADDED_SOME]: {
    icon: SCIcon`event`,
    fill: true,
    label: 'data.chips.add_events.ADDED_SOME',
    disabled: false,
    color: 'success',
  },
  [AddEventStates.REMOVED_ALL]: {
    icon: SCIcon`calendar_today`,
    fill: false,
    label: 'data.chips.add_events.REMOVED_ALL',
    disabled: false,
    color: 'primary',
  },
  [AddEventStates.UNAVAILABLE]: {
    icon: SCIcon`event_busy`,
    fill: false,
    label: 'data.chips.add_events.UNAVAILABLE',
    disabled: true,
    color: 'dark',
  },
};
