/*
 * Copyright (C) 2022 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {Component, Input} from '@angular/core';
import {SCDateSeries, SCThings, SCThingType} from '@openstapps/core';

/**
 * Shows a horizontal list of action chips
 */
@Component({
  selector: 'stapps-action-chip-list',
  templateUrl: 'action-chip-list.html',
  styleUrls: ['action-chip-list.scss'],
})
export class ActionChipListComponent {
  private _item: SCThings;

  /**
   * If chips are applicable
   */
  applicable: Record<string, boolean> = {};

  /**
   * The item the action belongs to
   */
  @Input() set item(item: SCThings) {
    this._item = item;

    this.applicable = {
      locate: false, // TODO: reimplement this at a later date
      event:
        item.type === SCThingType.AcademicEvent ||
        (item.type === SCThingType.DateSeries && (item as SCDateSeries).dates.length > 0),
    };
  }

  get item() {
    return this._item;
  }
}
