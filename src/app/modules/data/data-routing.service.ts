/*
 * Copyright (C) 2022 StApps
 *  This program is free software: you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the Free
 *  Software Foundation, version 3.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 *  more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {Injectable} from '@angular/core';
import {SCThings, SCThingWithoutReferences} from '@openstapps/core';
import {Subject} from 'rxjs';

/**
 * Transmits event of data selection
 */
@Injectable({
  providedIn: 'root',
})
export class DataRoutingService {
  /**
   * Provides the thing that was selected
   */
  private childSelectedEvent = new Subject<SCThings>();

  private pathSelectedEvent = new Subject<SCThingWithoutReferences>();

  /**
   * Provides the thing that was selected
   *
   * @param thing The selected thing
   */
  emitChildEvent(thing: SCThings) {
    this.childSelectedEvent.next(thing);
  }

  emitPathEvent(thing: SCThingWithoutReferences) {
    this.pathSelectedEvent.next(thing);
  }

  /**
   * Provides a listener for the event
   */
  itemSelectListener() {
    return this.childSelectedEvent.asObservable();
  }

  pathSelectListener() {
    return this.pathSelectedEvent.asObservable();
  }
}
