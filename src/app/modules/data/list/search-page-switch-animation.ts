/*
 * Copyright (C) 2022 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */

import type {AnimationBuilder} from '@ionic/angular';
import {AnimationController} from '@ionic/angular';
import type {AnimationOptions} from '@ionic/angular/providers/nav-controller';

/**
 *
 */
export function searchPageSwitchAnimation(animationController: AnimationController): AnimationBuilder {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  return (_baseElement: HTMLElement, options: AnimationOptions | any) => {
    const rootTransition = animationController
      .create()
      .duration(options.duration ?? 200)
      .easing('ease');

    const enterTransition = animationController
      .create()
      .fromTo('opacity', '0', '1')
      .addElement(options.enteringEl);
    const exitTransition = animationController
      .create()
      .fromTo('opacity', '1', '1')
      .addElement(options.leavingEl);
    console.log(options.enteringEl.querySelector('stapps-data-list'));
    const contentSlide = animationController
      .create()
      .fromTo('transform', 'translateX(600px)', 'translateX(0px)')
      .addElement(options.enteringEl.querySelector('stapps-data-list'));

    rootTransition.addAnimation([enterTransition, exitTransition, contentSlide]);
    return rootTransition;
  };
}
