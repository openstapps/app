/*
 * Copyright (C) 2023 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {Component, OnDestroy, OnInit} from '@angular/core';
import {MapPosition} from '../../map/position.service';
import {SearchPageComponent} from './search-page.component';
import {Geolocation} from '@capacitor/geolocation';
import {Subscription} from 'rxjs';

/**
 * Presents a list of places for eating/drinking
 */
@Component({
  templateUrl: 'search-page.html',
  styleUrls: ['../../data/list/search-page.scss'],
})
export class FoodDataListComponent extends SearchPageComponent implements OnInit, OnDestroy {
  title = 'canteens.title';

  showNavigation = false;

  locationWatch?: Subscription;

  /**
   * Sets the forced filter to present only places for eating/drinking
   */
  ngOnInit() {
    this.locationWatch?.unsubscribe();
    this.locationWatch = this.createLocationWatch();
    this.showDefaultData = true;

    this.sortQuery = [
      {
        arguments: {field: 'name'},
        order: 'asc',
        type: 'ducet',
      },
    ];

    this.forcedFilter = {
      arguments: {
        filters: [
          {
            arguments: {
              field: 'categories',
              value: 'canteen',
            },
            type: 'value',
          },
          {
            arguments: {
              field: 'categories',
              value: 'student canteen',
            },
            type: 'value',
          },
          {
            arguments: {
              field: 'categories',
              value: 'cafe',
            },
            type: 'value',
          },
          {
            arguments: {
              field: 'categories',
              value: 'restaurant',
            },
            type: 'value',
          },
        ],
        operation: 'or',
      },
      type: 'boolean',
    };

    if (this.positionService.position) {
      this.sortQuery = [
        {
          type: 'distance',
          order: 'asc',
          arguments: {
            field: 'geo',
            position: [this.positionService.position.longitude, this.positionService.position.latitude],
          },
        },
      ];
    }

    super.ngOnInit();
  }

  private createLocationWatch(): Subscription {
    return this.positionService
      .watchCurrentLocation(this.constructor.name, {enableHighAccuracy: false, maximumAge: 1000})
      .subscribe({
        next: (position: MapPosition) => {
          this.positionService.position = position;
        },
        error: async _error => {
          this.positionService.position = undefined;
          await Geolocation.checkPermissions();
        },
      });
  }

  async ionViewWillEnter() {
    await super.ionViewWillEnter();
    this.locationWatch?.unsubscribe();
    this.locationWatch = this.createLocationWatch();
  }

  ionViewWillLeave() {
    this.locationWatch?.unsubscribe();
  }

  ngOnDestroy() {
    super.ngOnDestroy();
    this.locationWatch?.unsubscribe();
  }
}
