/*
 * Copyright (C) 2022 StApps
 *  This program is free software: you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the Free
 *  Software Foundation, version 3.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 *  more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {Component, Input, TemplateRef} from '@angular/core';
import {SCThings, SCThingWithoutReferences, SCUuid} from '@openstapps/core';
import {Tree} from '../../../_helpers/collections/tree-group';
import {DataListContext} from './data-list.component';

@Component({
  selector: 'tree-list-fragment',
  templateUrl: 'tree-list-fragment.html',
  styleUrls: ['tree-list-fragment.scss'],
})
export class TreeListFragmentComponent {
  entries?: [string, Tree<SCThings>][];

  @Input() set items(items: Tree<SCThings> | undefined) {
    if (!items) {
      delete this.entries;
      return;
    }
    const temporary = items._;
    delete items._;
    this.entries = Object.entries(items) as [string, Tree<SCThings>][];
    items._ = temporary;
  }

  @Input() groupMap: Record<SCUuid, SCThingWithoutReferences>;

  @Input() singleType = false;

  @Input() listItemTemplateRef: TemplateRef<DataListContext<SCThings>>;
}
