/*
 * Copyright (C) 2023 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {Component, ContentChild, HostBinding, Input, TemplateRef} from '@angular/core';
import {SCThings} from '@openstapps/core';
import {DataRoutingService} from '../data-routing.service';
import {DataListContext} from './data-list.component';

/**
 * Shows data items in lists such es search result
 */
@Component({
  selector: 'stapps-data-list-item',
  styleUrls: ['data-list-item.scss'],
  templateUrl: 'data-list-item.html',
})
export class DataListItemComponent {
  /**
   * Whether the list item should show a thumbnail
   */
  @Input() hideThumbnail = false;

  /**
   * An item to show
   */
  @Input() item: SCThings;

  @Input() listItemEndInteraction = true;

  @Input() lines = 'inset';

  @Input() forceHeight = false;

  height?: string;

  @Input() appearance: 'normal' | 'square' = 'normal';

  @ContentChild(TemplateRef) contentTemplateRef: TemplateRef<DataListContext<SCThings>>;

  @HostBinding('class.square') get square() {
    return this.appearance === 'square';
  }

  constructor(private readonly dataRoutingService: DataRoutingService) {}

  /**
   * Emit event that an item was selected
   */
  notifySelect() {
    this.dataRoutingService.emitChildEvent(this.item);
  }
}
