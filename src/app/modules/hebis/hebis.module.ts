/*
 * Copyright (C) 2018-2022 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {ScrollingModule} from '@angular/cdk/scrolling';
import {CommonModule} from '@angular/common';
import {HttpClientModule} from '@angular/common/http';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {IonicModule} from '@ionic/angular';
import {TranslateModule} from '@ngx-translate/core';
import {MarkdownModule} from 'ngx-markdown';
import {MomentModule} from 'ngx-moment';
import {ThingTranslateModule} from '../../translation/thing-translate.module';
import {MenuModule} from '../menu/menu.module';
import {StorageModule} from '../storage/storage.module';
import {HebisDetailComponent} from './hebis-detail/hebis-detail.component';
import {HebisDetailContentComponent} from './hebis-detail/hebis-detail-content.component';
import {HebisSearchPageComponent} from './list/hebis-search-page.component';
import {HebisDataProvider} from './hebis-data.provider';
import {DaiaDataProvider} from './daia-data.provider';
import {StAppsWebHttpClient} from '../data/stapps-web-http-client.provider';
import {HebisRoutingModule} from './hebis-routing.module';
import {DataModule} from '../data/data.module';
import {DataListComponent} from '../data/list/data-list.component';
import {DaiaAvailabilityComponent} from './daia-availability/daia-availability.component';
import {UtilModule} from '../../util/util.module';
import {IonIconModule} from '../../util/ion-icon/ion-icon.module';
import {DaiaHoldingComponent} from './daia-availability/daia-holding.component';

/**
 * Module for handling data
 */
@NgModule({
  declarations: [
    HebisSearchPageComponent,
    HebisDetailComponent,
    HebisDetailContentComponent,
    DaiaAvailabilityComponent,
    DaiaHoldingComponent,
  ],
  entryComponents: [DataListComponent],
  imports: [
    CommonModule,
    DataModule,
    FormsModule,
    HebisRoutingModule,
    IonIconModule,
    HttpClientModule,
    IonicModule.forRoot(),
    MarkdownModule.forRoot(),
    MenuModule,
    MomentModule.forRoot({
      relativeTimeThresholdOptions: {
        m: 59,
      },
    }),
    ScrollingModule,
    StorageModule,
    TranslateModule.forChild(),
    ThingTranslateModule.forChild(),
    UtilModule,
  ],
  providers: [HebisDataProvider, DaiaDataProvider, StAppsWebHttpClient],
})
export class HebisModule {}
