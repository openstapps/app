/*
 * Copyright (C) 2022 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {Component} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';
import {SCUuid} from '@openstapps/core';
import {HebisDataProvider} from '../hebis-data.provider';
import {FavoritesService} from '../../favorites/favorites.service';
import {DataProvider} from '../../data/data.provider';
import {DataDetailComponent} from '../../data/detail/data-detail.component';
import {DaiaHolding} from '../protocol/response';
import {ModalController} from '@ionic/angular';

/**
 * A Component to display an SCThing detailed
 */
@Component({
  selector: 'stapps-hebis-detail',
  styleUrls: ['hebis-detail.scss'],
  templateUrl: 'hebis-detail.html',
})
export class HebisDetailComponent extends DataDetailComponent {
  holdings: DaiaHolding[];

  /**
   *
   * @param route the route the page was accessed from
   * @param dataProvider the data provider
   * @param favoritesService the favorites provider
   * @param modalController the modal controller
   * @param translateService he translate provider
   * @param hebisDataProvider HebisDataProvider
   */
  constructor(
    route: ActivatedRoute,
    dataProvider: DataProvider,
    favoritesService: FavoritesService,
    modalController: ModalController,
    translateService: TranslateService,
    private hebisDataProvider: HebisDataProvider,
  ) {
    super(route, dataProvider, favoritesService, modalController, translateService);
  }

  /**
   * Initialize
   */
  async ionViewWillEnter() {
    const uid = this.route.snapshot.paramMap.get('uid') || '';
    await this.getItem(uid ?? '', false);
  }

  /**
   * Provides data item with given UID
   *
   * @param uid Unique identifier of a thing
   * @param _forceReload Ignore any cached data
   */
  async getItem(uid: SCUuid, _forceReload: boolean) {
    this.hebisDataProvider.hebisSearch({query: uid, page: 0}).then(result => {
      // eslint-disable-next-line unicorn/no-null
      this.item = (result.data && result.data[0]) || null;
    });
  }
}
