/*
 * Copyright (C) 2022 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */

/* eslint-disable @typescript-eslint/no-non-null-assertion,@typescript-eslint/no-explicit-any */
import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {ComponentFixture, TestBed} from '@angular/core/testing';
import {ActivatedRoute, RouterModule} from '@angular/router';
import {TranslateLoader, TranslateModule, TranslateService} from '@ngx-translate/core';
import {sampleThingsMap} from '../../../_helpers/data/sample-things';
import {HebisRoutingModule} from '../hebis-routing.module';
import {HebisModule} from '../hebis.module';
import {DaiaAvailabilityComponent} from './daia-availability.component';
import {Observable, of} from 'rxjs';
import {StorageProvider} from '../../storage/storage.provider';
import {DaiaDataProvider} from '../daia-data.provider';
import {LoggerConfig, LoggerModule, NGXLogger} from 'ngx-logger';
import {ConfigProvider} from '../../config/config.provider';

const translations: any = {data: {detail: {TITLE: 'Foo'}}};

class TranslateFakeLoader implements TranslateLoader {
  getTranslation(_lang: string): Observable<any> {
    return of(translations);
  }
}

describe('DaiaAvailabilityComponent', () => {
  let comp: DaiaAvailabilityComponent;
  let fixture: ComponentFixture<DaiaAvailabilityComponent>;
  let dataProvider: DaiaDataProvider;
  const sampleThing = sampleThingsMap.book[0];
  let translateService: TranslateService;
  let configProviderMock: jasmine.SpyObj<ConfigProvider>;

  // @Component({ selector: 'stapps-data-list-item', template: '' })
  // class DataListItemComponent {
  //   @Input() item;
  // }

  const fakeActivatedRoute = {
    snapshot: {
      paramMap: {
        get: () => {
          return sampleThing.uid;
        },
      },
    },
  };

  const storageProviderSpy = jasmine.createSpyObj('StorageProvider', ['init', 'get', 'has', 'put', 'search']);

  beforeEach(() => {
    configProviderMock = jasmine.createSpyObj('ConfigProvider', ['init', 'getValue', 'getAnyValue']);
    TestBed.configureTestingModule({
      imports: [
        RouterModule.forRoot([], {relativeLinkResolution: 'legacy'}),
        HebisRoutingModule,
        HebisModule,
        TranslateModule.forRoot({
          loader: {provide: TranslateLoader, useClass: TranslateFakeLoader},
        }),
        LoggerModule,
      ],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: fakeActivatedRoute,
        },
        {
          provide: StorageProvider,
          useValue: storageProviderSpy,
        },
        {
          provide: ConfigProvider,
          useValue: configProviderMock,
        },
        NGXLogger,
        LoggerConfig,
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    }).compileComponents();
  });

  beforeEach(async () => {
    dataProvider = TestBed.inject(DaiaDataProvider);
    const workingDAIAurl = 'https://daia.hebis.de/DAIA2/UB_Frankfurt';
    dataProvider.daiaServiceUrl = workingDAIAurl;
    translateService = TestBed.inject(TranslateService);
    spyOn(dataProvider, 'getAvailability' as any).and.returnValue(Promise.resolve([]));
    spyOn(DaiaAvailabilityComponent.prototype, 'getAvailability').and.callThrough();
    fixture = await TestBed.createComponent(DaiaAvailabilityComponent);
    comp = fixture.componentInstance;
    translateService.use('foo');
    fixture.detectChanges();
  });

  it('should create component', () => expect(comp).toBeDefined());

  it('should get the availability of an item', () => {
    comp.getAvailability(sampleThing.uid);
    expect(DaiaAvailabilityComponent.prototype.getAvailability).toHaveBeenCalledWith(sampleThing.uid);
  });

  it('should get the availability of an item when the view is entered', () => {
    comp.ngOnInit();
    expect(DaiaAvailabilityComponent.prototype.getAvailability).toHaveBeenCalledWith(sampleThing.uid);
  });
});
