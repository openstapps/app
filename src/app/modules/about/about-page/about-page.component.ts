/*
 * Copyright (C) 2022 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {SCAboutPage, SCAppConfiguration} from '@openstapps/core';
import {ConfigProvider} from '../../config/config.provider';
import packageJson from '../../../../../package.json';
import config from 'capacitor.config';

@Component({
  selector: 'about-page',
  templateUrl: 'about-page.html',
  styleUrls: ['about-page.scss'],
})
export class AboutPageComponent implements OnInit {
  content: SCAboutPage;

  appName = config.appName;

  version = packageJson.version;

  constructor(private readonly route: ActivatedRoute, private readonly configProvider: ConfigProvider) {}

  async ngOnInit() {
    const route = this.route.snapshot.url.map(it => it.path).join('/');
    this.content =
      (this.configProvider.getValue('aboutPages') as SCAppConfiguration['aboutPages'])[route] ?? {};
  }
}
