/*
 * Copyright (C) 2022 StApps
 *  This program is free software: you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the Free
 *  Software Foundation, version 3.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 *  more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {IonicModule} from '@ionic/angular';
import {TranslateModule} from '@ngx-translate/core';
import {ThingTranslateModule} from '../../translation/thing-translate.module';
import {ConfigProvider} from '../config/config.provider';
import {AboutPageComponent} from './about-page/about-page.component';
import {MarkdownModule} from 'ngx-markdown';
import {AboutPageContentComponent} from './about-page/about-page-content.component';
import {AboutLicensesComponent} from './about-licenses.component';
import {DataModule} from '../data/data.module';
import {ScrollingModule} from '@angular/cdk/scrolling';
import {AboutLicenseModalComponent} from './about-license-modal.component';
import {AboutChangelogComponent} from './about-changelog.component';
import {UtilModule} from '../../util/util.module';
import {IonIconModule} from '../../util/ion-icon/ion-icon.module';

const settingsRoutes: Routes = [
  {path: 'about', component: AboutPageComponent},
  {path: 'about/changelog', component: AboutChangelogComponent},
  {path: 'about/imprint', component: AboutPageComponent},
  {path: 'about/privacy', component: AboutPageComponent},
  {path: 'about/terms', component: AboutPageComponent},
  {path: 'about/licenses', component: AboutLicensesComponent},
];

/**
 * Settings Module
 */
@NgModule({
  declarations: [
    AboutPageComponent,
    AboutPageContentComponent,
    AboutLicensesComponent,
    AboutLicenseModalComponent,
    AboutChangelogComponent,
  ],
  imports: [
    CommonModule,
    IonIconModule,
    FormsModule,
    IonicModule.forRoot(),
    TranslateModule.forChild(),
    ThingTranslateModule.forChild(),
    RouterModule.forChild(settingsRoutes),
    MarkdownModule,
    DataModule,
    ScrollingModule,
    UtilModule,
  ],
  providers: [ConfigProvider],
})
export class AboutModule {}
