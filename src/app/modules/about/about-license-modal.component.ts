/*
 * Copyright (C) 2021 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */

import {Component, Input} from '@angular/core';
import {License} from './about-licenses.component';

@Component({
  selector: 'about-license-modal',
  templateUrl: 'about-license-modal.html',
  styleUrls: ['about-license-modal.scss'],
})
export class AboutLicenseModalComponent {
  @Input() license: License;

  /**
   * Action when close is pressed
   */
  @Input() dismissAction: () => void;
}
