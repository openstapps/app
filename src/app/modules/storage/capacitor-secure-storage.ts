/*
 * Copyright (C) 2022 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {CapacitorSecureStorage} from 'ionic-appauth/lib/capacitor';

/*
 * Removes an item from storage before entering the new one to avoid issues
 * after iOS upgrade (iOS 16)
 */
export class SafeCapacitorSecureStorage extends CapacitorSecureStorage {
  async setItem(name: string, value: string): Promise<void> {
    if (!Storage) throw new Error('Capacitor Storage Is Undefined!');

    try {
      await super.removeItem(name);
    } catch {}
    return super.setItem(name, value);
  }
}
