/*
 * Copyright (C) 2023 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {RouterModule, Routes} from '@angular/router';
import {IonicModule} from '@ionic/angular';
import {TranslateModule} from '@ngx-translate/core';

import {ThingTranslateModule} from '../../translation/thing-translate.module';
import {ConfigProvider} from '../config/config.provider';
import {SettingsItemComponent} from './item/settings-item.component';
import {SettingsPageComponent} from './page/settings-page.component';
import {SettingTranslatePipe} from './setting-translate.pipe';
import {SettingsProvider} from './settings.provider';
import {CalendarSyncSettingsComponent} from './page/calendar-sync-settings.component';
import {ScheduleProvider} from '../calendar/schedule.provider';
import {ThingTranslatePipe} from '../../translation/thing-translate.pipe';
import {ScheduleSyncService} from '../background/schedule/schedule-sync.service';
import {CalendarService} from '../calendar/calendar.service';
import {CalendarModule} from '../calendar/calendar.module';
import {BackgroundModule} from '../background/background.module';
import {UtilModule} from '../../util/util.module';
import {IonIconModule} from '../../util/ion-icon/ion-icon.module';

const settingsRoutes: Routes = [{path: 'settings', component: SettingsPageComponent}];

/**
 * Settings Module
 */
@NgModule({
  declarations: [
    SettingsPageComponent,
    SettingsItemComponent,
    SettingTranslatePipe,
    CalendarSyncSettingsComponent,
  ],
  exports: [SettingsItemComponent, SettingTranslatePipe],
  imports: [
    CommonModule,
    FormsModule,
    CalendarModule,
    IonIconModule,
    BackgroundModule,
    IonicModule.forRoot(),
    TranslateModule.forChild(),
    ThingTranslateModule.forChild(),
    RouterModule.forChild(settingsRoutes),
    UtilModule,
  ],
  providers: [
    ScheduleSyncService,
    ConfigProvider,
    SettingsProvider,
    CalendarService,
    ScheduleProvider,
    ThingTranslatePipe,
  ],
})
export class SettingsModule {}
