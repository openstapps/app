/*
 * Copyright (C) 2022 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */

import {Pipe, PipeTransform} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {SCSetting} from '@openstapps/core';
import {ThingTranslatePipe} from '../../translation/thing-translate.pipe';
import {ThingTranslateService} from '../../translation/thing-translate.service';

/**
 * Translates a setting value (into the display value in current language)
 */
@Pipe({
  name: 'settingValueTranslate',
})
export class SettingTranslatePipe implements PipeTransform {
  constructor(
    private readonly translate: TranslateService,
    private readonly thingTranslate: ThingTranslateService,
  ) {}

  transform(setting: SCSetting) {
    const thingTranslatePipe = new ThingTranslatePipe(this.translate, this.thingTranslate);
    const translatedSettingValues = thingTranslatePipe.transform('values', setting);

    return translatedSettingValues
      ? translatedSettingValues[setting.values?.indexOf(setting.value as string) as number]
      : undefined;
  }
}
