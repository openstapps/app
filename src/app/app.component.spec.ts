/*
 * Copyright (C) 2022 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */

/* eslint-disable @typescript-eslint/no-explicit-any */
import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {TestBed} from '@angular/core/testing';

import {Platform} from '@ionic/angular';

import {TranslateService} from '@ngx-translate/core';
import {ThingTranslateService} from './translation/thing-translate.service';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {AppComponent} from './app.component';
import {AuthModule} from './modules/auth/auth.module';
import {ConfigProvider} from './modules/config/config.provider';
import {SettingsProvider} from './modules/settings/settings.provider';
import {NGXLogger} from 'ngx-logger';
import {RouterTestingModule} from '@angular/router/testing';
import {ScheduleSyncService} from './modules/background/schedule/schedule-sync.service';
import {sampleAuthConfiguration} from './_helpers/data/sample-configuration';
import {StorageProvider} from './modules/storage/storage.provider';
import {SimpleBrowser} from './util/browser.factory';

describe('AppComponent', () => {
  let platformReadySpy: any;
  let platformSpy: jasmine.SpyObj<Platform>;
  let translateServiceSpy: jasmine.SpyObj<TranslateService>;
  let thingTranslateServiceSpy: jasmine.SpyObj<ThingTranslateService>;
  let settingsProvider: jasmine.SpyObj<SettingsProvider>;
  let configProvider: jasmine.SpyObj<ConfigProvider>;
  let ngxLogger: jasmine.SpyObj<NGXLogger>;
  let scheduleSyncServiceSpy: jasmine.SpyObj<ScheduleSyncService>;
  let platformIsSpy;
  let storageProvider: jasmine.SpyObj<StorageProvider>;
  let simpleBrowser: jasmine.SpyObj<SimpleBrowser>;

  beforeEach(() => {
    platformReadySpy = Promise.resolve();
    platformIsSpy = Promise.resolve();
    platformSpy = jasmine.createSpyObj('Platform', {
      ready: platformReadySpy,
      is: platformIsSpy,
    });
    translateServiceSpy = jasmine.createSpyObj('TranslateService', ['setDefaultLang', 'use']);
    thingTranslateServiceSpy = jasmine.createSpyObj('ThingTranslateService', ['init']);
    settingsProvider = jasmine.createSpyObj('SettingsProvider', [
      'getSettingValue',
      'provideSetting',
      'setCategoriesOrder',
    ]);
    scheduleSyncServiceSpy = jasmine.createSpyObj('ScheduleSyncService', [
      'getDifferences',
      'postDifferencesNotification',
    ]);
    configProvider = jasmine.createSpyObj('ConfigProvider', ['init', 'getAnyValue']);
    configProvider.getAnyValue = jasmine.createSpy().and.callFake(function () {
      return sampleAuthConfiguration;
    });
    ngxLogger = jasmine.createSpyObj('NGXLogger', ['log', 'error', 'warn']);
    storageProvider = jasmine.createSpyObj('StorageProvider', ['init', 'get', 'has', 'put']);
    simpleBrowser = jasmine.createSpyObj('SimpleBrowser', ['open']);

    TestBed.configureTestingModule({
      imports: [RouterTestingModule.withRoutes([]), HttpClientTestingModule, AuthModule],
      declarations: [AppComponent],
      providers: [
        {provide: Platform, useValue: platformSpy},
        {provide: TranslateService, useValue: translateServiceSpy},
        {provide: ThingTranslateService, useValue: thingTranslateServiceSpy},
        {provide: ScheduleSyncService, useValue: scheduleSyncServiceSpy},
        {provide: SettingsProvider, useValue: settingsProvider},
        {provide: ConfigProvider, useValue: configProvider},
        {provide: NGXLogger, useValue: ngxLogger},
        {provide: StorageProvider, useValue: storageProvider},
        {provide: SimpleBrowser, useValue: simpleBrowser},
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    }).compileComponents();
  });

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });

  it('should initialize the app', async () => {
    TestBed.createComponent(AppComponent);
    expect(platformSpy.ready).toHaveBeenCalled();
    // await platformReadySpy;

    // TODO: https://capacitorjs.com/docs/guides/mocking-plugins
    // expect(splashScreenSpy.hide).toHaveBeenCalled();
  });

  // TODO: add more tests!
});
