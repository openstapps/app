/*
 * Copyright (C) 2022 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */

import {TranslatePipe} from '@ngx-translate/core';
import {ActionSheetOptions, AlertOptions} from '@ionic/angular';

export enum PendingChangesRole {
  SAVE = 'save',
  DISCARD = 'discard',
  CANCEL = 'cancel',
}

/**
 *
 */
export function pendingChangesActionSheet(
  translatePipe: TranslatePipe,
  includeSaveOption = true,
): ActionSheetOptions & AlertOptions {
  return {
    header: translatePipe.transform('modal.dismiss_warn_pending_changes.TITLE'),
    buttons: [
      ...(includeSaveOption
        ? [
            {
              text: translatePipe.transform('modal.dismiss_warn_pending_changes.SAVE'),
              role: PendingChangesRole.SAVE,
            },
          ]
        : []),
      {
        text: translatePipe.transform('modal.dismiss_warn_pending_changes.CANCEL'),
        role: PendingChangesRole.CANCEL,
      },
      {
        text: translatePipe.transform('modal.dismiss_warn_pending_changes.DISCARD'),
        role: PendingChangesRole.DISCARD,
      },
    ],
  };
}
