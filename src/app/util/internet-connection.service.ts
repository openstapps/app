/*
 * Copyright (C) 2023 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {fromEvent, merge, ObservableInput, of, race, RetryConfig, share, Subject, takeUntil} from 'rxjs';
import {Injectable} from '@angular/core';
import {filter, map, startWith, take, tap} from 'rxjs/operators';
import {NGXLogger} from 'ngx-logger';
import {Router} from '@angular/router';

@Injectable({
  providedIn: 'root',
})
export class InternetConnectionService {
  private readonly manualRetry$ = new Subject<void>();

  private readonly abortRetry$ = new Subject<void>();

  /**
   * Emits whenever the browser goes online or offline.
   */
  readonly offline$ = window
    ? merge(
        fromEvent(window, 'online').pipe(map(() => false)),
        fromEvent(window, 'offline').pipe(map(() => true)),
      ).pipe(startWith(!window.navigator.onLine), share())
    : of(true);

  /**
   * Emits whenever http requests should be retried
   *
   * Also keeps track of when a retry is needed, automatically
   * registering itself.
   */
  readonly retryConfig: RetryConfig = {
    delay: this.doRetry.bind(this),
  };

  private doRetry(error: unknown, retryCount: number): ObservableInput<unknown> {
    return race(
      this.offline$.pipe(
        tap(it => console.log(it)),
        filter(it => !it),
        take(1),
      ),
      this.manualRetry$,
    ).pipe(
      tap({
        subscribe: () => {
          this.errors.add(error);
          if (this.errors.size > 0) {
            this.error$.next(true);
          }
        },
        next: () => {
          this.logger.error(`${retryCount}x`, error);
        },
        unsubscribe: () => {
          this.errors.delete(error);
          if (this.errors.size === 0) {
            this.error$.next(false);
          }
        },
      }),
      takeUntil(
        merge(
          this.abortRetry$.pipe(tap(() => this.logger.warn('HTTP Request retry aborted manually'))),
          this.router.events.pipe(tap(() => this.logger.warn('HTTP Request retry aborted by routing'))),
        ),
      ),
    );
  }

  /**
   * Emits when there are errors
   */
  readonly error$ = new Subject<boolean>();

  private readonly errors = new Set<unknown>();

  constructor(private readonly logger: NGXLogger, private readonly router: Router) {}

  /**
   * Retry all failed http requests
   */
  retry() {
    this.manualRetry$.next();
  }

  /**
   * Abandon all failed http requests
   */
  dismissError() {
    this.abortRetry$.next();
  }
}
