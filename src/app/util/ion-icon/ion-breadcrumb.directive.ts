/*
 * Copyright (C) 2022 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */

import {Directive, ElementRef, ViewContainerRef} from '@angular/core';
import {SCIcon} from './icon';
import {IconReplacer} from './replace-util';

@Directive({
  selector: 'ion-breadcrumb',
})
export class IonBreadcrumbDirective extends IconReplacer {
  constructor(element: ElementRef, viewContainerRef: ViewContainerRef) {
    super(element, viewContainerRef, 'shadow');
  }

  replace() {
    this.replaceIcon(
      this.host.querySelector('span[part="separator"]'),
      {
        name: SCIcon`arrow_forward_ios`,
        size: 16,
        style: `color: var(--ion-color-tint);`,
      },
      '-separator',
    );
    this.replaceIcon(
      this.host.querySelector('button[part="collapsed-indicator"]'),
      {
        name: SCIcon`more_horiz`,
        size: 24,
      },
      '-collapsed',
    );
  }
}
