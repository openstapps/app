/*
 * Copyright (C) 2023 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */

import {ChangeDetectionStrategy, Component, HostBinding, Input} from '@angular/core';

@Component({
  selector: 'stapps-icon',
  templateUrl: 'icon.html',
  styleUrls: ['icon.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class IconComponent {
  @HostBinding('style.--size')
  @Input()
  size?: number;

  @HostBinding('style.--weight')
  @Input()
  weight?: number;

  @HostBinding('style.--grade')
  @Input()
  grade?: number;

  @Input()
  fill: boolean;

  @HostBinding('innerHtml')
  @Input()
  name: string;

  @HostBinding('style.--fill') get fillStyle(): number | undefined {
    return this.fill ? 1 : undefined;
  }

  @HostBinding('class.material-symbols-rounded') hostClass = true;
}
