/*
 * Copyright (C) 2022 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */

import {Directive, ElementRef, Host, Optional, Self, ViewContainerRef} from '@angular/core';
import {SCIcon} from './icon';
import {IconReplacer} from './replace-util';
import {TranslateService} from '@ngx-translate/core';
import {Subscription} from 'rxjs';
import {IonBackButton} from '@ionic/angular';
import {TitleCasePipe} from '@angular/common';

@Directive({
  selector: 'ion-back-button',
})
export class IonBackButtonDirective extends IconReplacer {
  private subscriptions: Subscription[] = [];

  constructor(
    element: ElementRef,
    viewContainerRef: ViewContainerRef,
    @Host() @Self() @Optional() private ionBackButton: IonBackButton,
    private translateService: TranslateService,
    private titleCasePipe: TitleCasePipe,
  ) {
    super(element, viewContainerRef, 'shadow');
  }

  replace() {
    this.replaceIcon(this.host.querySelector('.button-inner'), {
      md: SCIcon`arrow_back`,
      ios: SCIcon`arrow_back_ios`,
      size: 24,
    });
  }

  init() {
    this.subscriptions.push(
      this.translateService.stream('back').subscribe((value: string) => {
        this.ionBackButton.text = this.titleCasePipe.transform(value);
      }),
    );
  }

  destroy() {
    for (const subscription of this.subscriptions) {
      subscription.unsubscribe();
    }
  }
}
