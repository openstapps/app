/*
 * Copyright (C) 2022 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */

import {Directive, ElementRef, ViewContainerRef} from '@angular/core';
import {SCIcon} from './icon';
import {IconReplacer} from './replace-util';

@Directive({
  selector: 'ion-reorder',
})
export class IonReorderDirective extends IconReplacer {
  constructor(element: ElementRef, viewContainerRef: ViewContainerRef) {
    super(element, viewContainerRef, 'shadow');
  }

  replace() {
    this.replaceIcon(this.host, {
      name: SCIcon`reorder`,
      size: 24,
    });
  }
}
