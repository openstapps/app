/*
 * Copyright (C) 2022 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
/* eslint-disable unicorn/no-null */

import {matchPropertyContent, matchTagProperties} from './icon-match';

describe('matchTagProperties', function () {
  const regex = matchTagProperties('test');

  it('should match html tag content', function () {
    expect('<test content></test>'.match(regex)).toEqual([' content']);
  });

  it('should match all tags', function () {
    expect('<test content1></test> <test content2></test>'.match(regex)).toEqual([' content1', ' content2']);
  });

  it('should not match wrong tags', function () {
    expect('<no content></no>'.match(regex)).toEqual(null);
  });

  it('should accept valid html whitespaces', function () {
    expect(
      `
    <test
 content
 >
     </test
        >
    `.match(regex),
    ).toEqual(['\n content\n ']);
  });
});

describe('matchPropertyContent', function () {
  const regex = matchPropertyContent(['test1', 'test2']);

  it('should match bare literals', function () {
    expect(`test1="content" test2="content1"`.match(regex)).toEqual(['content', 'content1']);
  });

  it('should match angular literals', function () {
    expect(`[test1]="'content'" [test2]="'content1'"`.match(regex)).toEqual(['content', 'content1']);
  });

  it('should not match wrong literals', function () {
    expect(`no="content" [no]="'content'"`.match(regex)).toEqual(null);
  });
});
