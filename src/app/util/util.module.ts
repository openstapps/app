/*
 * Copyright (C) 2023 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {NgModule} from '@angular/core';
import {ArrayLastPipe} from './array-last.pipe';
import {DateIsThisPipe} from './date-is-today.pipe';
import {NullishCoalescingPipe} from './nullish-coalecing.pipe';
import {DateFromIndexPipe} from './date-from-index.pipe';
import {DaytimeKeyPipe} from './daytime-key.pipe';
import {LazyPipe} from './lazy.pipe';
import {NextDateInListPipe} from './next-date-in-list.pipe';
import {EditModalComponent} from './edit-modal.component';
import {BrowserModule} from '@angular/platform-browser';
import {IonicModule} from '@ionic/angular';
import {TranslateModule} from '@ngx-translate/core';
import {ElementSizeChangeDirective} from './element-size-change.directive';
import {OpeningHoursComponent} from './opening-hours.component';
import {ThingTranslateModule} from '../translation/thing-translate.module';
import {SimpleSwiperComponent} from './simple-swiper.component';
import {SearchbarAutofocusDirective} from './searchbar-autofocus.directive';
import {SectionComponent} from './section.component';
import {RouterModule} from '@angular/router';
import {IonContentParallaxDirective} from './ion-content-parallax.directive';

@NgModule({
  imports: [BrowserModule, IonicModule, TranslateModule, ThingTranslateModule.forChild(), RouterModule],
  declarations: [
    IonContentParallaxDirective,
    ElementSizeChangeDirective,
    ArrayLastPipe,
    DateIsThisPipe,
    NullishCoalescingPipe,
    LazyPipe,
    SectionComponent,
    DateFromIndexPipe,
    DaytimeKeyPipe,
    NextDateInListPipe,
    EditModalComponent,
    OpeningHoursComponent,
    SimpleSwiperComponent,
    SearchbarAutofocusDirective,
  ],
  exports: [
    IonContentParallaxDirective,
    ElementSizeChangeDirective,
    ArrayLastPipe,
    DateIsThisPipe,
    NullishCoalescingPipe,
    LazyPipe,
    DateFromIndexPipe,
    DaytimeKeyPipe,
    SectionComponent,
    NextDateInListPipe,
    EditModalComponent,
    OpeningHoursComponent,
    SimpleSwiperComponent,
    SearchbarAutofocusDirective,
  ],
})
export class UtilModule {}
