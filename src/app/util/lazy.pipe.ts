/*
 * Copyright (C) 2022 StApps
 *  This program is free software: you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the Free
 *  Software Foundation, version 3.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 *  more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  this program. If not, see <https://www.gnu.org/licenses/>.
 */

import {Observable} from 'rxjs';
import {Pipe, PipeTransform} from '@angular/core';
import {SCSaveableThing, SCThings, SCUuid} from '@openstapps/core';
import {DataProvider, DataScope} from '../modules/data/data.provider';
import {get} from '../_helpers/collections/get';

@Pipe({
  name: 'lazyThing',
  pure: true,
})
export class LazyPipe implements PipeTransform {
  constructor(private readonly dataProvider: DataProvider) {}

  transform(
    uid: SCUuid,
    path?: keyof SCThings | string,
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
  ): Observable<SCThings | SCSaveableThing> | any {
    return new Observable(subscriber => {
      this.dataProvider.get(uid, DataScope.Remote).then(it => {
        subscriber.next(path ? get(it, path) : it);
        subscriber.complete();
      });
    });
  }
}
