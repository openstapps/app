/*
 * Copyright (C) 2021 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */

import {Injectable, Pipe, PipeTransform} from '@angular/core';
import moment from 'moment';

/**
 * Return the extended translation key by the current daytime key
 */
@Injectable()
@Pipe({
  name: 'daytimeKey',
})
export class DaytimeKeyPipe implements PipeTransform {
  /**
   * Transform
   */
  transform(translationKey: string): string {
    const hour = Number.parseInt(moment().format('HH'), 10);
    let key = '';
    if (hour >= 5 && hour <= 10) {
      key = 'morning';
    } else if (hour >= 11 && hour <= 18) {
      key = 'day';
    } else if (hour >= 19 && hour <= 23) {
      key = 'evening';
    } else {
      key = 'night';
    }

    return `${translationKey}_${key}`;
  }
}
