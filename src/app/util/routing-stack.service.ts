/*
 * Copyright (C) 2022 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */

import {Injectable} from '@angular/core';
import {NavigationEnd, Router} from '@angular/router';
import {SCSaveableThing, SCThings} from '@openstapps/core';
import {DataProvider, DataScope} from '../modules/data/data.provider';

@Injectable({
  providedIn: 'root',
})
export class RoutingStackService {
  currentRoute: string;

  currentDataDetail?: Promise<SCSaveableThing | SCThings>;

  lastRoute: string;

  lastDataDetail?: Promise<SCSaveableThing | SCThings>;

  constructor(private router: Router, private dataProvider: DataProvider) {
    this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        this.lastRoute = this.currentRoute;
        this.currentRoute = event.urlAfterRedirects;

        const uid = this.currentRoute.match(/^\/data-detail\/([\w-]+)$/)?.[1];
        this.lastDataDetail = this.currentDataDetail;
        this.currentDataDetail = uid ? this.dataProvider.get(uid, DataScope.Remote) : undefined;
      }
    });
  }
}
