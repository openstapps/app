/*
 * Copyright (C) 2023 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */

import {ModuleWithProviders, NgModule, Provider} from '@angular/core';
import {
  ArrayJoinPipe,
  DateLocalizedFormatPipe,
  DurationLocalizedPipe,
  EntriesPipe,
  IsNaNPipe,
  IsNumericPipe,
  MetersLocalizedPipe,
  NumberLocalizedPipe,
  OpeningHoursPipe,
  SentenceCasePipe,
  StringSplitPipe,
  ToUnixPipe,
} from './common-string-pipes';
import {ThingTranslateDefaultParser, ThingTranslateParser} from './thing-translate.parser';
import {ThingTranslatePipe} from './thing-translate.pipe';
import {ThingTranslateService} from './thing-translate.service';
import {IonIconModule} from '../util/ion-icon/ion-icon.module';
import {TranslateSimplePipe} from './translate-simple.pipe';
import {PropertyNameTranslatePipe} from './property-name-translate.pipe';

export interface ThingTranslateModuleConfig {
  parser?: Provider;
}

@NgModule({
  imports: [IonIconModule],
  declarations: [
    ArrayJoinPipe,
    DurationLocalizedPipe,
    NumberLocalizedPipe,
    MetersLocalizedPipe,
    StringSplitPipe,
    PropertyNameTranslatePipe,
    ThingTranslatePipe,
    TranslateSimplePipe,
    DateLocalizedFormatPipe,
    OpeningHoursPipe,
    SentenceCasePipe,
    ToUnixPipe,
    EntriesPipe,
    IsNaNPipe,
    IsNumericPipe,
  ],
  exports: [
    IonIconModule,
    ArrayJoinPipe,
    DurationLocalizedPipe,
    NumberLocalizedPipe,
    MetersLocalizedPipe,
    StringSplitPipe,
    PropertyNameTranslatePipe,
    ThingTranslatePipe,
    TranslateSimplePipe,
    DateLocalizedFormatPipe,
    OpeningHoursPipe,
    SentenceCasePipe,
    ToUnixPipe,
    EntriesPipe,
    IsNaNPipe,
    IsNumericPipe,
  ],
})
export class ThingTranslateModule {
  /**
   * Use this method in your other (non root) modules to import the directive/pipe
   */
  static forChild(config: ThingTranslateModuleConfig = {}): ModuleWithProviders<ThingTranslateModule> {
    return {
      ngModule: ThingTranslateModule,
      providers: [
        config.parser ?? {
          provide: ThingTranslateParser,
          useClass: ThingTranslateDefaultParser,
        },
        ThingTranslateService,
      ],
    };
  }

  /**
   * Use this method in your root module to provide the TranslatorService
   */
  static forRoot(config: ThingTranslateModuleConfig = {}): ModuleWithProviders<ThingTranslateModule> {
    return {
      ngModule: ThingTranslateModule,
      providers: [
        config.parser ?? {
          provide: ThingTranslateParser,
          useClass: ThingTranslateDefaultParser,
        },
        ThingTranslateService,
      ],
    };
  }
}
