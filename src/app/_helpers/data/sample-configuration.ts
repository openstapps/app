/*
 * Copyright (C) 2023 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {
  SCAboutPageContentType,
  SCAuthorizationProvider,
  SCBackendAggregationConfiguration,
  SCIndexResponse,
  SCSettingInputType,
  SCThingOriginType,
  SCThingType,
} from '@openstapps/core';
import {Polygon} from 'geojson';
import packageJson from '../../../../package.json';

// provides sample aggregations to be used in tests or backendless development
export const sampleAggregations: SCBackendAggregationConfiguration[] = [
  {
    fieldName: 'categories',
    onlyOnTypes: [
      SCThingType.AcademicEvent,
      SCThingType.Article,
      SCThingType.Building,
      SCThingType.Catalog,
      SCThingType.Dish,
      SCThingType.PointOfInterest,
      SCThingType.Room,
    ],
  },
  {
    fieldName: 'inPlace.name',
    onlyOnTypes: [
      SCThingType.DateSeries,
      SCThingType.Dish,
      SCThingType.Floor,
      SCThingType.Organization,
      SCThingType.PointOfInterest,
      SCThingType.Room,
      SCThingType.Ticket,
    ],
  },
  {
    fieldName: 'academicTerms.acronym',
    onlyOnTypes: [SCThingType.AcademicEvent, SCThingType.SportCourse],
  },
  {
    fieldName: 'academicTerm.acronym',
    onlyOnTypes: [SCThingType.Catalog],
  },
  {
    fieldName: 'majors',
    onlyOnTypes: [SCThingType.AcademicEvent],
  },
  {
    fieldName: 'keywords',
    onlyOnTypes: [SCThingType.Article, SCThingType.Book, SCThingType.Message, SCThingType.Video],
  },
  {
    fieldName: 'type',
  },
];

export const sampleAuthConfiguration: {
  default: SCAuthorizationProvider;
  paia: SCAuthorizationProvider;
} = {
  default: {
    client: {clientId: '', scopes: '', url: ''},
    endpoints: {
      authorization: '',
      mapping: {id: '', name: ''},
      token: '',
      userinfo: '',
    },
  },
  paia: {
    client: {clientId: '', scopes: '', url: ''},
    endpoints: {
      authorization: '',
      mapping: {id: '', name: ''},
      token: '',
      userinfo: '',
    },
  },
};

export const sampleDefaultPolygon: Polygon = {
  coordinates: [
    [
      [8.660432999690723, 50.123027017044436],
      [8.675496285518358, 50.123027017044436],
      [8.675496285518358, 50.13066176448642],
      [8.660432999690723, 50.13066176448642],
      [8.660432999690723, 50.123027017044436],
    ],
  ],
  type: 'Polygon',
};

const scVersion = packageJson.dependencies['@openstapps/core'];

export const sampleIndexResponse: SCIndexResponse = {
  app: {
    aboutPages: {
      about: {
        title: 'About',
        content: [
          {
            value: 'This is the about page',
            type: SCAboutPageContentType.MARKDOWN,
            translations: {
              en: {
                value: 'This is the about page',
              },
            },
          },
        ],
        translations: {
          en: {
            title: 'About',
          },
        },
      },
    },
    campusPolygon: {
      coordinates: [[[1, 2]], [[1, 2]]],
      type: 'Polygon',
    },
    features: {},
    menus: [
      {
        icon: 'icon',
        items: [
          {
            icon: 'icon',
            route: '/index',
            title: 'start',
            translations: {
              de: {
                title: 'Start',
              },
              en: {
                title: 'start',
              },
            },
          },
        ],
        title: 'main',
        route: '/main',
        translations: {
          de: {
            title: 'Haupt',
          },
          en: {
            title: 'main',
          },
        },
      },
    ],
    name: 'StApps',
    privacyPolicyUrl: 'foo.bar',
    settings: [
      {
        categories: ['credentials'],
        defaultValue: '',
        inputType: SCSettingInputType.Text,
        name: 'username',
        order: 0,
        origin: {
          indexed: '2018-09-11T12:30:00Z',
          name: 'Dummy',
          type: SCThingOriginType.Remote,
        },
        translations: {
          de: {
            name: 'Benutzername',
          },
          en: {
            name: 'Username',
          },
        },
        type: SCThingType.Setting,
        uid: '',
      },
    ],
  },
  auth: {},
  backend: {
    SCVersion: scVersion,
    externalRequestTimeout: 5000,
    hiddenTypes: [SCThingType.DateSeries, SCThingType.Diff, SCThingType.Floor],
    mappingIgnoredTags: [],
    maxMultiSearchRouteQueries: 5,
    maxRequestBodySize: 512 * 1024,
    name: 'Technische Universität Berlin',
    namespace: '909a8cbc-8520-456c-b474-ef1525f14209',
    sortableFields: [
      {
        fieldName: 'name',
        sortTypes: ['ducet'],
      },
      {
        fieldName: 'type',
        sortTypes: ['ducet'],
      },
      {
        fieldName: 'categories',
        onlyOnTypes: [
          SCThingType.AcademicEvent,
          SCThingType.Building,
          SCThingType.Catalog,
          SCThingType.Dish,
          SCThingType.PointOfInterest,
          SCThingType.Room,
        ],
        sortTypes: ['ducet'],
      },
      {
        fieldName: 'geo',
        onlyOnTypes: [SCThingType.Building, SCThingType.PointOfInterest, SCThingType.Room],
        sortTypes: ['distance'],
      },
      {
        fieldName: 'geo',
        onlyOnTypes: [SCThingType.Building, SCThingType.PointOfInterest, SCThingType.Room],
        sortTypes: ['distance'],
      },
      {
        fieldName: 'inPlace.geo',
        onlyOnTypes: [
          SCThingType.DateSeries,
          SCThingType.Dish,
          SCThingType.Floor,
          SCThingType.Organization,
          SCThingType.PointOfInterest,
          SCThingType.Room,
          SCThingType.Ticket,
        ],
        sortTypes: ['distance'],
      },
      {
        fieldName: 'offers',
        onlyOnTypes: [SCThingType.Dish],
        sortTypes: ['price'],
      },
    ],
  },
};
