/*
 * Copyright (C) 2022 StApps
 *  This program is free software: you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the Free
 *  Software Foundation, version 3.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 *  more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {sum, sumBy} from './sum';

describe('sum', () => {
  it('should return the sum of all elements in the collection', () => {
    const collection = [1, 2, 3, 4, 5];
    const result = sum(collection);
    expect(result).toBe(15);
  });
});

describe('sumBy', function () {
  it('should return the sum of all elements in the collection', () => {
    const collection = [{a: 1}, {a: 2}, {a: 3}, {a: 4}, {a: 5}];
    const result = sumBy(collection, it => it.a);
    expect(result).toBe(15);
  });
});
