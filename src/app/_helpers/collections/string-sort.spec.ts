/*
 * Copyright (C) 2022 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */

import {stringSort, stringSortBy} from './string-sort';

describe('stringSort', () => {
  it('should sort an array of strings', () => {
    expect(['a', 'c', 'b', 'd'].sort(stringSort)).toEqual(['a', 'b', 'c', 'd']);
  });
});

describe('stringSortBy', () => {
  it('should sort an array of strings', () => {
    expect([{item: 'a'}, {item: 'c'}, {item: 'b'}, {item: 'd'}].sort(stringSortBy(it => it.item))).toEqual([
      {item: 'a'},
      {item: 'b'},
      {item: 'c'},
      {item: 'd'},
    ]);
  });
});
