/*
 * Copyright (C) 2022 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */

export type Tree<T> = {
  [key: string]: Tree<T>;
} & {
  _?: T[] | undefined;
};

/**
 *
 */
export function treeGroupBy<T>(items: T[], transform: (item: T) => string[]): Tree<T> {
  const tree: Tree<T> = {};

  for (const item of items) {
    let currentTree = tree;
    const keys = transform(item);
    if (keys.length === 0) {
      currentTree._ = currentTree._ || [];
      currentTree._.push(item);
    }
    for (const [i, key] of keys.entries()) {
      currentTree = currentTree[key] = (currentTree[key] ?? {}) as Tree<T>;
      if (i === keys.length - 1) {
        currentTree._ = currentTree._ ?? [];
        // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
        currentTree._.push(item);
      }
    }
  }

  return tree;
}
