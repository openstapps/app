/*
 * Copyright (C) 2022 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * Partitions a list into two lists. One with the elements that satisfy a predicate,
 * and one with the elements that don't satisfy the predicate.
 */
export function partition<T>(array: T[], transform: (item: T) => boolean): [T[], T[]] {
  return array.reduce<[T[], T[]]>(
    (accumulator, item) => {
      accumulator[transform(item) ? 0 : 1].push(item);
      return accumulator;
    },
    [[], []],
  );
}
