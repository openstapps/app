/*
 * Copyright (C) 2022 StApps
 *  This program is free software: you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the Free
 *  Software Foundation, version 3.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 *  more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  this program. If not, see <https://www.gnu.org/licenses/>.
 */

import {differenceBy} from './difference';

describe('differenceBy', function () {
  it('should return the difference of two arrays', function () {
    const a = [1, 2, 3, 4, 5];
    const b = [1, 2, 3];

    expect(differenceBy(a, b, it => it)).toEqual([4, 5]);
  });
});
