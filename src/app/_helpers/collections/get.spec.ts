/*
 * Copyright (C) 2022 StApps
 *  This program is free software: you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the Free
 *  Software Foundation, version 3.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 *  more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {get} from './get';

describe('get', function () {
  it('should get a simple path', function () {
    const object = {
      a: {
        b: {
          c: 'd',
        },
      },
    };
    expect(get(object, 'a.b.c')).toBe('d');
  });

  it('should return undefined for a non-existent path', function () {
    const object = {
      a: {
        b: {
          c: 'd',
        },
      },
    };
    expect(get(object, 'a.b.c.d')).toBeUndefined();
  });
});
