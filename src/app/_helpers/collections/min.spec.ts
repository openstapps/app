/*
 * Copyright (C) 2022 StApps
 *  This program is free software: you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the Free
 *  Software Foundation, version 3.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 *  more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  this program. If not, see <https://www.gnu.org/licenses/>.
 */

import {minBy} from './min';

describe('minBy', function () {
  it('should pick the minimum value based on transform', function () {
    expect(
      minBy(
        [
          {id: 1, name: 'A'},
          {id: 2, name: 'B'},
          {id: 3, name: 'C'},
        ],
        it => it.id,
      ),
    ).toEqual({id: 1, name: 'A'});
  });

  it('should not return undefined if there are other choices', function () {
    expect(
      minBy(
        [
          {id: undefined, name: 'B'},
          {id: 1, name: 'A'},
          {id: undefined, name: 'C'},
        ],
        it => it.id,
      ),
    ).toEqual({id: 1, name: 'A'});
  });
});
