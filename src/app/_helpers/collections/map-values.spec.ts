/*
 * Copyright (C) 2022 StApps
 *  This program is free software: you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the Free
 *  Software Foundation, version 3.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 *  more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  this program. If not, see <https://www.gnu.org/licenses/>.
 */

import {mapValues} from './map-values';

describe('map-values', () => {
  it('should map values', () => {
    const object = {
      a: 1,
      b: 2,
      c: 3,
    };

    const result = mapValues(object, value => value * 2);

    expect(result).toEqual({
      a: 2,
      b: 4,
      c: 6,
    });
  });

  it('should not modify the original object', () => {
    const object = {
      a: 1,
      b: 2,
      c: 3,
    };

    mapValues(object, value => value * 2);

    expect(object).toEqual({
      a: 1,
      b: 2,
      c: 3,
    });
  });
});
