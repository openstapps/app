/*
 * Copyright (C) 2022 StApps
 *  This program is free software: you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the Free
 *  Software Foundation, version 3.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 *  more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  this program. If not, see <https://www.gnu.org/licenses/>.
 */

import {Tree, treeGroupBy} from './tree-group';

interface TestItem {
  id: number;
  path?: string[];
}

describe('tree-group', function () {
  it('should create a tree', function () {
    const items: Array<TestItem> = [
      {
        id: 1,
        path: ['a', 'b', 'c'],
      },
      {
        id: 2,
        path: ['a', 'b', 'd'],
      },
    ];

    const tree = treeGroupBy(items, item => item.path ?? []);

    const expectedTree: Tree<TestItem> = {
      a: {
        b: {
          c: {_: [items[0]]},
          d: {_: [items[1]]},
        } as Tree<TestItem>,
      } as Tree<TestItem>,
    } as Tree<TestItem>;

    expect(tree).toEqual(expectedTree);
  });

  it('should also sort empty paths', () => {
    const items: Array<TestItem> = [
      {
        id: 1,
        path: ['a', 'b', 'c'],
      },
      {
        id: 2,
      },
    ];

    const tree = treeGroupBy(items, item => item.path ?? []);

    const expectedTree: Tree<TestItem> = {
      a: {
        b: {
          c: {_: [items[0]]},
        } as Tree<TestItem>,
      } as Tree<TestItem>,
      _: [items[1]],
    } as Tree<TestItem>;

    expect(tree).toEqual(expectedTree);
  });
});
