/*
 * Copyright (C) 2022 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * Create an object composed of keys generated from the results of running
 * each element of collection thru iteratee. The corresponding value of
 * each key is the last element responsible for generating the key. The
 * iteratee is invoked with one argument: (value).
 */
export function keyBy<T>(collection: T[], key: (item: T) => string | number): Record<string, T> {
  return collection.reduce((accumulator, item) => {
    accumulator[key(item)] = item;
    return accumulator;
  }, {} as Record<string | number, T>);
}
