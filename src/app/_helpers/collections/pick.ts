/*
 * Copyright (C) 2022 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * Pick a set of properties from an object
 */
export function pick<T extends object, U extends keyof T>(object: T, keys: U[]): Pick<T, U> {
  return keys.reduce((accumulator, key) => {
    if (object.hasOwnProperty(key)) {
      accumulator[key] = object[key];
    }
    return accumulator;
  }, {} as Pick<T, U>);
}

/**
 * Pick a set of properties from an object using a predicate function
 */
export function pickBy<T extends object, U extends keyof T>(
  object: T,
  predicate: (value: T[U], key: U) => boolean,
): Pick<T, U> {
  return (Object.keys(object) as U[]).reduce((accumulator, key) => {
    if (predicate(object[key], key)) {
      accumulator[key] = object[key];
    }
    return accumulator;
  }, {} as Pick<T, U>);
}
