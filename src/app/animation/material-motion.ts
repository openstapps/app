/*
 * Copyright (C) 2022 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {animate, sequence, state, style, transition, trigger} from '@angular/animations';

/**
 * Fade transition
 *
 * @see {@link https://material.io/design/motion/the-motion-system.html#fade}
 */
export const materialFade = trigger('materialFade', [
  state('in', style({opacity: 1})),
  transition(':enter', [style({opacity: 0}), animate('250ms ease')]),
  transition(':leave', [animate('200ms ease', style({opacity: 0}))]),
]);

/**
 * Fade transition
 *
 * @see {@link https://material.io/design/motion/the-motion-system.html#fade}
 */
export const materialManualFade = trigger('materialManualFade', [
  state('in', style({opacity: 1})),
  state('out', style({opacity: 0})),
  transition('in => out', animate('200ms ease')),
  transition('out => in', animate('250ms ease')),
]);

/**
 * Fade through transition
 *
 * @see {@link https://material.io/design/motion/the-motion-system.html#fade-through}
 */
export const materialFadeThrough = trigger('materialFadeThrough', [
  state('in', style({transform: 'scale(100%)', opacity: 1})),
  transition(':enter', [style({transform: 'scale(80%)', opacity: 0}), animate('250ms ease')]),
  transition(':leave', [animate('200ms ease', style({opacity: 0}))]),
]);

export const SHARED_AXIS_DIRECTIONS = {
  [-1]: 'go-backward',
  [0]: 'in',
  [1]: 'go-forward',
};

/**
 * Shared axis transition along the X-Axis
 *
 * Needs to be manually choreographed
 *
 * @see {@link https://material.io/design/motion/the-motion-system.html#shared-axis}
 * @see {SharedAxisChoreographer}
 */
export const materialSharedAxisX = trigger('materialSharedAxisX', [
  state(SHARED_AXIS_DIRECTIONS[-1], style({opacity: 0, transform: 'translateX(30px)'})),
  state(SHARED_AXIS_DIRECTIONS[0], style({opacity: 1, transform: 'translateX(0px)'})),
  state(SHARED_AXIS_DIRECTIONS[1], style({opacity: 0, transform: 'translateX(-30px)'})),
  transition(
    `${SHARED_AXIS_DIRECTIONS[-1]} => ${SHARED_AXIS_DIRECTIONS[0]}`,
    sequence([style({opacity: 0, transform: 'translateX(-30px)'}), animate('100ms ease-out')]),
  ),
  transition(`${SHARED_AXIS_DIRECTIONS[0]} => *`, animate('100ms ease-out')),
  transition(
    `${SHARED_AXIS_DIRECTIONS[1]} => ${SHARED_AXIS_DIRECTIONS[0]}`,
    sequence([style({opacity: 0, transform: 'translateX(30px)'}), animate('100ms ease-out')]),
  ),
]);
