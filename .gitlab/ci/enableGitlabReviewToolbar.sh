#!/usr/bin/env sh

# Adds a preedefined script tag including the merge current request id to the angular web app index.html.
# This enables the interactive gitlab review toolbar.

MERGE_REQUEST_ID=""
if echo -n $3 | grep -Eq '[0-9]+$'; then
  MERGE_REQUEST_ID="$(echo -n "$3" | grep -Eo '[0-9]+$')"
fi

INDEX_PATH=$(dirname $1)
SCRIPT_TAG="<script defer data-project-id=\"$2\" data-project-path=\"openstapps/app\" data-merge-request-id=\"$MERGE_REQUEST_ID\" data-mr-url=\"https://gitlab.com\" id=\"review-app-toolbar-script\" src=\"visual_review_toolbar.js\"></script>"

curl https://gitlab.com/assets/webpack/visual_review_toolbar.js --output  "$INDEX_PATH/visual_review_toolbar.js" --silent
sed -i -e "\@</head>@i\\$SCRIPT_TAG" $1
