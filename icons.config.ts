/*
 * Copyright (C) 2022 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import type {IconConfig} from './scripts/icon-config';

const config: IconConfig = {
  inputPath: 'node_modules/material-symbols/material-symbols-rounded.woff2',
  outputPath: 'src/assets/icons.min.woff2',
  htmlGlob: 'src/**/*.html',
  scriptGlob: 'src/**/*.ts',
  additionalIcons: {
    about: ['copyright', 'campaign', 'policy', 'description', 'text_snippet'],
    navigation: [
      'home',
      'newspaper',
      'search',
      'calendar_month',
      'local_cafe',
      'local_library',
      'inventory_2',
      'map',
      'grade',
      'account_circle',
      'settings',
      'info',
      'rate_review',
    ],
  },
  codePoints: {
    ios_share: 'e6b8',
    fact_check: 'f0c5',
  },
};

export default config;
