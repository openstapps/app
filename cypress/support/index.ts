/*
 * Copyright (C) 2023 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */

// ***********************************************************
// This example support/index.js is processed and
// loaded automatically before your test files.
//
// This is a great place to put global configuration and
// behavior that modifies Cypress.
//
// You can change the location of this file or turn off
// automatically serving support files with the
// 'supportFile' configuration option.
//
// You can read more here:
// https://on.cypress.io/configuration
// ***********************************************************

// When a command from ./commands is ready to use, import with `import './commands'` syntax
// import './commands';

beforeEach(async function () {
  let databases: string[];
  if (window.indexedDB.databases) {
    databases = (await window.indexedDB.databases()).map(it => it.name);
    console.log('Trying to clear all databases');
  } else {
    console.log("Browser doesn't support database enumeration, deleting just ionic storage");
    databases = ['_ionicstorage'];
  }
  for (const database of databases) {
    if (database) {
      console.log(`Deleting database ${database}`);
      await new Promise(resolve => (window.indexedDB.deleteDatabase(database).onsuccess = resolve));
      console.log(`Deleted database ${database}`);
    }
  }
});

Cypress.on('window:before:load', window => {
  // Fake that user is using its browser in german language
  Object.defineProperty(window.navigator, 'language', {value: 'de-DE'});
  Object.defineProperty(window.navigator, 'languages', [{value: 'de-DE'}]);

  // Fail tests on console error
  cy.stub(window.console, 'error').callsFake(message => {
    // log out to the terminal
    cy.now('task', 'error', message);
    // log to Command Log and fail the test
    throw new Error(message);
  });
});

Cypress.on('uncaught:exception', error => {
  return !error.message.includes('ResizeObserver loop limit exceeded');
});
