/*
 * Copyright (C) 2023 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */

describe('assessments', function () {
  /*it('should have default back navigation', function () {
    // TODO: Implement this
    cy.visit(
      'assessments/detail/02f065a6-6c02-58ab-97d9-a3febdbc91a1?token=mock',
    );
    cy.get('ion-back-button').click();
  });*/

  it('should always have a path', function () {
    cy.visit('/assessments/detail/02f065a6-6c02-58ab-97d9-a3febdbc91a1?token=mock');

    cy.get('stapps-data-path').should('contain', 'Basismodule').should('contain', 'Modellierung');
  });

  it('should have a collapsed path', function () {
    cy.visit('/assessments/detail/02f065a6-6c02-58ab-97d9-a3febdbc91a1?token=mock');

    cy.get('.breadcrumb-collapsed').click();
    cy.get('ion-breadcrumb').should('have.length', 3);
  });
});
