/*
 * Copyright (C) 2023 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */

/* eslint-disable @typescript-eslint/no-explicit-any */
//import {ScheduleProvider} from '../../src/app/modules/calendar/schedule.provider';

describe('dashboard', async function () {
  describe('schedule section', function () {
    it('should lead to the schedule', function () {
      cy.visit('/overview');
      cy.get('.schedule').contains('a', 'Stundenplan').click();
      cy.url().should('include', '/schedule/recurring');

      cy.visit('/overview');
      cy.get('.schedule').contains('a', 'Kein Eintrag gefunden').click();
      cy.url().should('include', '/schedule/recurring');
    });

    // TODO: Reenable and stabilize tests
    //it('should display the next unit', function () {
    //  let angular: any;
    //  cy.intercept('POST', 'https://mobile.server.uni-frankfurt.de/search', {
    //    fixture: 'search/types/date-series/date-series-1.json',
    //  }).as('search');
    //  cy.visit('/overview');
    //  cy.get('.schedule-item-button').should('exist');
    //  cy.window()
    //    .then(win => (angular = (win as any).ng))
    //    .then(() =>
    //      cy.get('app-dashboard').then($dashboard => {
    //        const appComponent = angular.getComponent($dashboard[0]);
    //        const scheduleProvider =
    //          appComponent.scheduleProvider as ScheduleProvider;

    //        scheduleProvider.restore(['abc']);
    //      }),
    //    );
    //  cy.wait('@search');
    //  cy.visit('/overview');
    //  cy.get('.schedule-item-button').should('contain', 'UNIcert (Test)');
    //});
  });

  describe('mensa section', function () {
    it('should have info when nothing is added', function () {
      cy.visit('/overview');

      cy.get('stapps-mensa-section').within(() => {
        cy.get('swiper').should('not.exist');
        cy.get('.nothing-selected > ion-label > a').should('have.text', 'Übersicht der Mensen');
      });
    });

    it('should add a mensa', function () {
      cy.clock(new Date('2022-06-08'), ['Date']);
      cy.visit('/overview');
      cy.get('stapps-mensa-section').find('.nothing-selected > ion-label > a').click();
      cy.intercept('POST', 'https://mobile.server.uni-frankfurt.de/search', {
        fixture: 'search/types/canteen/canteen-search-result.json',
      });
      cy.get('stapps-favorite-button').first().click();
      cy.intercept('POST', 'https://mobile.server.uni-frankfurt.de/search/multi', {
        fixture: 'search/types/dish/dish-2.json',
      });
      cy.get('ion-back-button').click();
      cy.get('stapps-mensa-section').find('simple-swiper > *').should('have.length.greaterThan', 1);
    });
  });

  describe('news section', function () {
    beforeEach(function () {
      cy.intercept('POST', 'https://mobile.server.uni-frankfurt.de/search', {
        fixture: 'search/types/message/message-1.json',
      }).as('search');
    });

    // TODO: Cypress has no real way of setting the presence of a pointing device,
    //       which means the behavior is undefined and depends on the testing device
    // it('should have desktop navigation buttons', function () {
    //   cy.visit('/overview');
    //
    //   cy.get('stapps-news-section').within(function () {
    //     cy.get('.swiper-button').should('not.have.css', 'display: none');
    //   });
    // });

    // it('should not have desktop navigation buttons on mobile', function () {
    //   cy.visit('/overview');
    //
    //   cy.get('stapps-news-section').within(function () {
    //     cy.get('.swiper-button').should('have.css', 'display: none');
    //   });
    // });

    it('should have working desktop navigation', function () {
      cy.visit('/overview');

      cy.get('stapps-news-section').within(function () {
        cy.get('simple-swiper > *').eq(0).should('be.visible');

        // TODO: see tests above, button will be visible or invisible
        //       depending on the testing device
        cy.get('.swiper-button > ion-button').eq(1).click({scrollBehavior: false, force: true});

        cy.get('simple-swiper > *').eq(0).should('not.be.visible');
      });
    });

    it('should have a link to the news page', function () {
      cy.visit('/overview');
      cy.wait('@search');
      cy.intercept('POST', 'https://mobile.server.uni-frankfurt.de/search', {
        fixture: 'search/types/message/single-message.json',
      }).as('search');

      cy.get('stapps-news-section').contains('ion-item', 'Mehr Nachrichten').click();
      cy.url().should('include', '/news');
    });
  });

  // TODO: Reenable tests after update of component
  // describe('navigation section', function () {
  //   it('should have editable dashboard sections', function () {
  //     cy.visit('/overview');

  //     const section = 'Vorlesungsv.';

  //     cy.get('stapps-navigation-section').within(() => {
  //       cy.get('.card').should('contain.text', section);

  //       cy.get('ion-icon[name=edit_square]').click();
  //     });

  //     cy.get('stapps-dashboard-edit-modal').within(() => {
  //       cy.contains('ion-item', section).find('ion-toggle').click();

  //       cy.contains('ion-button', 'Bestätigen').click();
  //     });

  //     cy.get('stapps-navigation-section').within(() => {
  //       cy.get('.card').should('not.contain.text', section);

  //       cy.get('ion-icon[name=edit_square]').click({scrollBehavior: false});
  //     });

  //     cy.get('stapps-dashboard-edit-modal').within(() => {
  //       cy.contains('ion-item', section).find('ion-toggle').click();

  //       cy.contains('ion-button', 'Bestätigen').click();
  //     });

  //     cy.get('stapps-navigation-section')
  //       .find('.card')
  //       .should('contain.text', section);
  //   });
  // });

  describe('search section', function () {
    it('should go to search', function () {
      cy.visit('/overview');

      cy.get('ion-searchbar').click({scrollBehavior: 'center'});
      cy.url().should('eq', Cypress.config().baseUrl + '/search');
      cy.get('ion-searchbar').should('not.have.value');
      cy.get('ion-searchbar input.searchbar-input').should('have.focus');

      cy.get('stapps-data-list-item').should('have.length', 0);
    });
  });
});
