/*
 * Copyright (C) 2022 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */

describe('feedback', function () {
  it('should send feedback', function () {
    cy.intercept('POST', 'https://mobile.server.uni-frankfurt.de/*').as('feedback');

    cy.visit('/feedback');

    cy.get('input[name=name]').type('test');
    cy.get('input[name=email]').type('aaa@bbb.com');
    cy.get('textarea[name=message]').type(Array.from({length: 50}, () => 'a').join(''));

    cy.get('ion-button[type=submit]').should('have.attr', 'disabled');
    cy.get('ion-checkbox[name=termsAgree]').click();
    cy.get('ion-button[type=submit]').should('not.have.attr', 'disabled');

    // cy.get('ion-button[type=submit]').click();
    // cy.wait('@feedback');
  });
});
