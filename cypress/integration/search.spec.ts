/*
 * Copyright (C) 2022 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */

describe('search', function () {
  beforeEach(function () {
    cy.intercept('POST', 'https://mobile.server.uni-frankfurt.de/search', {
      fixture: 'search/test.json',
    });
    cy.intercept('POST', 'https://mobile.server.uni-frankfurt.de/search/multi', {
      fixture: 'search/multi-result.json',
    });
  });

  it('should have search results', function () {
    cy.visit('/search');
    cy.get('ion-searchbar').type('test');
    cy.get('stapps-data-list-item').should('have.length.greaterThan', 1);
  });

  it('should display an error message when no results are found', function () {
    cy.visit('/search');
    cy.intercept('POST', 'https://mobile.server.uni-frankfurt.de/search', {
      fixture: 'search/no-results.json',
    });
    cy.get('ion-searchbar').type(Array.from({length: 10}, () => 'a').join(''));
    cy.get('stapps-data-list-item').should('have.length', 0);
    cy.get('stapps-data-list').contains('Keine Ergebnisse');
  });

  it('should have a working clear button', function () {
    cy.visit('/search');
    cy.get('ion-searchbar').type('test');
    cy.get('ion-searchbar').should('have.value', 'test');
    cy.get('stapps-data-list-item').should('have.length.greaterThan', 1);
    cy.get('.searchbar-clear-button').click();
    cy.get('ion-searchbar').should('have.value', '');
  });
});
