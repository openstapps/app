/*
 * Copyright (C) 2022 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */

describe('schedule', function () {
  beforeEach(function () {
    cy.intercept('POST', 'https://mobile.server.uni-frankfurt.de/search', {
      fixture: 'search/types/academic-event/event-1.json',
    }).as('search');
    cy.intercept('POST', 'https://mobile.server.uni-frankfurt.de/search/multi', {
      fixture: 'search/types/date-series/date-series-for-event-1.json',
    });
  });

  it('should respect the url', function () {
    cy.visit('/schedule/calendar/2022-01-19');
    cy.get('#date-select-button0').should('contain', '19.01.22');
  });

  it('should navigate a full page', function () {
    cy.visit('/schedule/calendar/2022-01-19');

    cy.get('.swiper-slide-active').should('contain', 'Mi');

    cy.get('.left-button').click();
    cy.wait(2000);
    cy.get('.swiper-slide-active').should('contain', 'So');

    cy.get('.right-button').click();
    cy.wait(2000);
    cy.get('.swiper-slide-active').should('contain', 'Mi');

    cy.get('.right-button').click();
    cy.wait(2000);
    cy.get('.swiper-slide-active').should('contain', 'Sa');
  });

  for (const [width, count] of [
    [760, 3],
    [770, 3],
    [1700, 7],
  ]) {
    const slideMultiplier = 3;
    it(`should have ${count} slides for ${width}px`, function () {
      cy.visit('/schedule/calendar/2022-01-59');
      cy.viewport(width, 550);
      cy.get('.schedule-wrapper > .infinite-swiper-wrapper')
        .find('.swiper-slide')
        .should('have.length', slideMultiplier * count)
        .first()
        .invoke('outerWidth')
        .should('be.gt', 140);
    });
  }

  it('should navigate to a specific date', function () {
    cy.visit('/schedule/calendar/2059-01-19');
    cy.contains('#date-select-button0', '19.01.59').click();
    cy.wait(2000);
    cy.get('button[data-day=1][data-month=1][data-year=2059]', {
      includeShadowDom: true,
    }).click();
    cy.wait(2000);
    cy.contains('#date-select-button0', '01.01.59').click();
  });

  // TODO: Reenable and stabilize tests
  //it('should add events', function () {
  //  cy.visit('/schedule/calendar/2059-01-19');
  //  cy.get('stapps-schedule-card').should('not.exist');

  //  cy.get('ion-fab-button').click();
  //  cy.wait(2000);
  //  cy.get('ion-modal').within(() => {
  //    cy.get('ion-searchbar').click().type('test');
  //    cy.contains('ion-item', 'UNIcert (Test)')
  //      .contains('stapps-add-event-action-chip', 'Termine Auswählen')
  //      .click();
  //    cy.wait(2000);
  //  });

  //  cy.intercept('POST', 'https://mobile.server.uni-frankfurt.de/search', {
  //    fixture: 'search/types/date-series/date-series-1.json',
  //  });

  //  cy.get('ion-app > ion-modal').within(() => {
  //    cy.contains('ion-item', /eine Stunde um 19. Jan. 2059, \d+:00/).click();
  //    cy.wait(2000);
  //    cy.contains('ion-button', 'Bestätigen').click();
  //    cy.wait(2000);
  //  });

  //  cy.get('ion-modal').within(() => {
  //    cy.contains('ion-item', 'UNIcert (Test)')
  //      .contains('stapps-add-event-action-chip', 'Hinzugefügt')
  //      .should('exist');
  //    cy.contains('ion-button', 'Schließen').click();
  //    cy.wait(2000);
  //  });

  //  cy.get('stapps-schedule-card').should('exist');
  //});
});
