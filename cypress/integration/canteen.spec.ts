/*
 * Copyright (C) 2023 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */

describe('canteen', function () {
  beforeEach(function () {
    cy.intercept('POST', 'https://mobile.server.uni-frankfurt.de/search', {
      fixture: 'search/types/canteen/canteen-1.json',
    }).as('search');
    cy.intercept('POST', 'https://mobile.server.uni-frankfurt.de/search/multi', {
      fixture: 'search/types/dish/dish-1.json',
    });
  });

  it('should not utilize the default price', function () {
    cy.visit('/data-detail/86464b64-da1e-5578-a5c4-eec23457f596');
    cy.contains('4,40 €').should('not.exist');
  });

  it('should have a student price', function () {
    cy.visit('/settings');
    cy.contains('stapps-settings-item', 'Gruppe').find('ion-select').should('be.visible').click();
    cy.get('ion-popover').contains('ion-item', 'Studierende').click();
    cy.wait(2000);
    cy.visit('/data-detail/86464b64-da1e-5578-a5c4-eec23457f596');
    cy.contains('3,30 €').should('exist');
  });

  it('should have an employee price', function () {
    cy.visit('/settings');
    cy.contains('stapps-settings-item', 'Gruppe').find('ion-select').should('be.visible').click();
    cy.get('ion-popover').contains('ion-item', 'Angestellte').click();
    cy.wait(2000);
    cy.visit('/data-detail/86464b64-da1e-5578-a5c4-eec23457f596');
    cy.contains('1,10 €').should('exist');
  });

  it('should have a guest price', function () {
    cy.visit('/settings');
    cy.contains('stapps-settings-item', 'Gruppe').find('ion-select').should('be.visible').click();
    cy.get('ion-popover').contains('ion-item', 'Gäste').click();
    cy.wait(2000);
    cy.visit('/data-detail/86464b64-da1e-5578-a5c4-eec23457f596');
    cy.contains('2,20 €').should('exist');
  });
});
