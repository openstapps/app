/*
 * Copyright (C) 2023 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
describe('App', () => {
  it('should have a proper title', () => {
    cy.visit('/');

    cy.title().should('equal', 'StApps');
  });

  it('should have a proper working navigation', () => {
    cy.visit('/');

    cy.contains('ion-tab-button', 'Mensa').click();
    cy.get('ion-title').contains('Mensa');
  });
});
