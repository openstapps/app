/*
 * Copyright (C) 2023 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */

describe('ical', function () {
  beforeEach(function () {
    cy.intercept('POST', 'https://mobile.server.uni-frankfurt.de/search', {
      fixture: 'search/types/academic-event/event-1.json',
    }).as('search');
    cy.intercept('POST', 'https://mobile.server.uni-frankfurt.de/search/multi', {
      fixture: 'search/types/date-series/date-series-for-event-1.json',
    });
  });

  it('should export a single event', function () {
    cy.visit('/search?query=test');
    cy.wait('@search');
    cy.contains('ion-chip', 'Termine Auswählen').first().click();

    cy.get('ion-app > ion-modal').within(() => {
      cy.get('ion-footer > ion-toolbar > ion-button').should('have.attr', 'disabled');
      cy.contains('ion-item', /19\.\s+Januar\s+2059,\s+\d{2}:00\s+-\s+\d{2}:00/).click();
      cy.get('ion-footer > ion-toolbar > ion-button').should('not.have.attr', 'disabled');
      cy.get('ion-footer > ion-toolbar > ion-button').click();
    });

    cy.get('add-event-review-modal').within(() => {
      cy.get('ion-item-group').should('contain', 'UNIcert (Test)');
      cy.contains('ion-item-group', /19\.\s+Jan\.\s+2059,\s+\d{2}:00/);
    });
  });
});
