/*
 * Copyright (C) 2023 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */

describe('context menu', function () {
  beforeEach(function () {
    cy.visit('/search');
    cy.intercept('POST', 'https://mobile.server.uni-frankfurt.de/search', {
      fixture: 'search/test.json',
    }).as('search');
    cy.intercept('POST', 'https://mobile.server.uni-frankfurt.de/search/multi', {
      fixture: 'search/multi-result.json',
    });
    cy.get('ion-searchbar').type('test');
    cy.wait('@search');
    cy.get('ion-searchbar > ion-menu-button').click();
  });

  it('should sort', function () {
    cy.get('stapps-context').within(() => {
      cy.contains('ion-item', 'Name').click();
      cy.wait('@search');
    });
  });

  it('should filter', function () {
    cy.get('stapps-context').within(() => {
      cy.contains('ion-item', '(27) Akademische Veranstaltung').click();
      cy.wait('@search');
    });
  });

  it('should have a working delete button', function () {
    cy.get('stapps-context').within(() => {
      cy.contains('ion-item', '(27) Akademische Veranstaltung').click();

      cy.get('.checkbox-checked').should('have.length', 1);
      cy.contains('ion-list-header', 'Filter').find('ion-button').click();
      cy.wait('@search');
      cy.get('.checkbox-checked').should('have.length', 0);
    });
  });

  it('should truncate categories', function () {
    cy.get('stapps-context').within(() => {
      cy.contains('ion-item', '(1) Universitätsveranstaltung').should('not.exist');
      cy.get('.context-filter > ion-button').click();
      cy.contains('ion-item', '(4) Universitätsveranstaltung').should('exist');
    });
  });

  it('should truncate long category items', function () {
    cy.contains('ion-list', 'Akademische Veranstaltung / Kategorien').within(() => {
      cy.contains('ion-item', '(1) Tutorium').should('not.exist');
      cy.get('div > ion-button').click();
      cy.contains('ion-item', '(1) Tutorium').should('exist');
    });
  });
});
