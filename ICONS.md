# Icons

A few notes to our icon set, for users and future maintainers.

## Usage

To find icon names, visit the
[Google Material Symbols Page](https://fonts.google.com/icons?icon.style=Rounded)

We have extended the `ion-icon` element via a directive. **Make sure your
module imports the `IonIconModule`**, you can then proceed using `ion-icon`s
as usual.

The modified `ion-icon` comes with a few extra features:

- `[fill]` controls the fill color of the icon.
- `[weight]` controls the font weight of the icon.
- `[size]` controls the font size of the icon.
- `[grade]` controls the font grade of the icon.

All of these attributes are animated as described
[here](https://developers.google.com/fonts/docs/material_symbols).

![](/readme-resources/fill-axis.gif)

You can also control these attributes via css:

```scss
ion-icon ::ng-deep stapps-icon {
  --fill: 1;
  --grade: 0;
  --weight: 400;
}
```

Sometimes icon code points cannot be determined automatically, for whatever
reason. In this case, you will need to specify the code point manually in
the config file.

### Icon Font Minification

Icon font minification is done automatically, but requires you to
follow a few simple rules:

1. Use the tagged template literal for referencing icon names in
   TypeScript files and code

```ts
SCIcon`icon_name`;
```

2. When using `ion-icon` in HTML, reference either icons that went through
   the `SCIcon` tag or write them as one of the following:

```html
<!-- do -->
<ion-icon name="icon_name"></ion-icon>
<ion-icon [name]="'icon_name'"></ion-icon>
<!-- don't -->
<ion-icon name="icon_name" />
<!-- self-closing -->
<ion-icon [name]="condition ? 'icon_name' : 'other_icon_name'"></ion-icon>
```

Icons that are unknown at compile time can be specified in the
`additionalIcons` property of the `icons.config.ts` file.

The minification can then be done by running

```shell
npm run minify-icons
```

Unfortunately, I was unable to find a JS package that could to the job,
and had to rely on the Python module [fonttools](https://github.com/fonttools/fonttools).

That means that you might run into additional issues when running the
above-mentioned command.
